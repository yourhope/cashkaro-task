package jsk.cashkaro.framework.worker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.activity.SharedActivity;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;

/**
 * Created by Sathish on 5/25/2021
 */
public class CKWorker extends Worker {

    Context mContext;

    public CKWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
    }

    /*
     * This method is responsible for doing the work
     * so whatever work that is needed to be performed
     * we will put it here
     *
     * For example, here I am calling the method displayNotification()
     * It will display a notification
     * So that we will understand the work is executed
     * */

    @NonNull
    @Override
    public Result doWork() {
        displayNotification();
        return Result.success();
    }

    /*
     * The method is doing nothing but only generating
     * a simple notification
     * If you are confused about it
     * you should check the Android Notification Tutorial
     * */
    private void displayNotification() {
        Logger.i("Notification triggerd");
        int mNotificationId = 001;
        String CHANNEL_ID = mContext.getString(R.string.app_name);
        Intent intent = new Intent(mContext, SharedActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH; //Important for heads-up notification
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "name", importance);
            channel.setShowBadge(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_notiication_banner);
        Notification notification = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentTitle(mContext.getString(R.string.app_name))
                .setContentText("Here is the Notification")
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(DEFAULT_SOUND)
                //.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_cash_karo))
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                        .setBigContentTitle(mContext.getString(R.string.sampleStr)))
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notification);
    }
}
