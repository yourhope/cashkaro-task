package jsk.cashkaro.framework.ui.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import jsk.cashkaro.common.Constants;
import jsk.cashkaro.task.R;


/**
 * Created by Sathish on 8/6/2017.
 */

public class CKButton extends AppCompatButton {
    public CKButton(Context context) {
        super(context);
        init();
    }

    public CKButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CKButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.FileName.defaultFontName);
        setTypeface(tf, Typeface.NORMAL);
        setTextSize(getResources().getDimensionPixelSize(R.dimen.size_7dp));
        setBackgroundResource(R.drawable.btn_bg);
        setTextColor(getResources().getColor(R.color.appWhite));
        setAllCaps(false);
    }
}

