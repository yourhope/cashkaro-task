package jsk.cashkaro.framework.ui.model;

/**
 * Created by Sathish on 5/23/2021
 */
public class TaskDataItem extends TaskItemModel {

    public TaskDataItem() {

    }

    public TaskDataItem(int viewType, String name) {
        this.viewType = viewType;
        this.name = name;
    }

    public TaskDataItem(int viewType, String imgUrl, String nameStr) {
        this.viewType = viewType;
        this.name = nameStr;
        this.imgUrl = imgUrl;
    }

    public TaskDataItem(int viewType, String imgUrl, String nameStr, String ageStr, String msgStr, String lastLoginStr) {
        this.viewType = viewType;
        this.imgUrl = imgUrl;
        this.name = nameStr;
        this.age = ageStr;
        this.message = msgStr;
        this.lastLogin = lastLoginStr;
    }

    public TaskDataItem(int viewType, String imgUrl, String msgStr, String dateStr, String nameStr) {
        this.viewType = viewType;
        this.imgUrl = imgUrl;
        this.name = nameStr;
        this.message = msgStr;
        this.date = dateStr;
    }
}
