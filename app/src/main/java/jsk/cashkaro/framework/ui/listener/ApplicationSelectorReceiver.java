package jsk.cashkaro.framework.ui.listener;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jsk.cashkaro.database.DatabaseClient;
import jsk.cashkaro.database.TaskEntity;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.ui.utils.CKUtils;

/**
 * Created by Sathish on 5/24/2021
 */
public class ApplicationSelectorReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String lAppName = "<unknown>";
        for (String key : Objects.requireNonNull(intent.getExtras()).keySet()) {
            try {
                ComponentName componentInfo = (ComponentName) intent.getExtras().get(key);
                PackageManager packageManager = context.getPackageManager();
                if (componentInfo != null)
                    lAppName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(componentInfo.getPackageName(), PackageManager.GET_META_DATA));
            } catch (Exception e) {
                //e.printStackTrace();
                // do nothing
            }
        }
        Bundle bundle = intent.getExtras();
        String lTitle = bundle.getString("title");
        String lTime = bundle.getString("time");
        String lImgUrl = bundle.getString("imgUrl");
        String lAppNameCopy = lAppName;
        /*Logger.i("Store to Room AppName: " + lAppName);
        Logger.i("Store to Room title : " + bundle.getString("title"));
        Logger.i("Store to Room time : " + bundle.getString("time"));
        Logger.i("Store to Room image URL : " + bundle.getString("imgUrl"));*/

        //Storing the data
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {
            //Background work here
            //creating a task
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setAppName(lAppNameCopy);
            taskEntity.setImgUrl(lImgUrl);
            taskEntity.setTitle(lTitle);
            taskEntity.setTime(lTime);

            //adding to database
            DatabaseClient.getInstance(context.getApplicationContext()).getAppDatabase()
                    .taskDao()
                    .insert(taskEntity);
            handler.post(() -> {
                //UI Thread work here
                CKUtils.showToast(context.getApplicationContext(), "Saved..");
            });
        });

    }
}
