package jsk.cashkaro.framework.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.checkbox.MaterialCheckBox;

import java.util.ArrayList;
import java.util.List;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.ui.listener.SelectionListener;
import jsk.cashkaro.framework.ui.utils.CKUtils;
import jsk.cashkaro.framework.ui.views.CKCustomTextView;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.model.SharedDataModel;

/**
 * Created by Sathish on 5/25/2021
 */
public class CKSharedRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final List<SharedDataModel> mDataList;
    private final List<SharedDataModel> mSelectedList;
    private SelectionListener mSelectionListener;

    public CKSharedRecyclerAdapter(Context context, List<SharedDataModel> dataList, SelectionListener selectionListener) {
        mContext = context;
        mDataList = dataList;
        mSelectedList = new ArrayList<>();
        mSelectionListener = selectionListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.shared_list_item, parent, false);
        return new SharedItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        SharedDataModel dataModel = mDataList.get(position);
        SharedItemViewHolder viewHolder = (SharedItemViewHolder) holder;
        viewHolder.titleTv.setText(dataModel.getTitle());
        viewHolder.appNameTv.setText("Shared Via : " + dataModel.getAppName());
        viewHolder.timeTv.setText("Time : " + dataModel.getTime());
        viewHolder.urlTv.setText("Img URL : " + dataModel.getImgUrl());

        viewHolder.checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (viewHolder.checkBox.isChecked()) {
                dataModel.setSelected(true);
                Logger.i("state selected");
                mSelectedList.add(dataModel);
                mSelectionListener.isSelected();
            } else {
                dataModel.setSelected(false);
                Logger.i("state unselected");
                mSelectedList.remove(dataModel);
                mSelectionListener.isSelected();
            }
        });

        if (dataModel.isSelected()) {
            viewHolder.checkBox.setChecked(true);
        } else {
            viewHolder.checkBox.setChecked(false);
        }

        holder.itemView.setOnClickListener(v -> {
            if (viewHolder.checkBox.isChecked())
                viewHolder.checkBox.setChecked(false);
            else
                viewHolder.checkBox.setChecked(true);
        });

        Glide.with(mContext).load(dataModel.getImgUrl())
                .apply(CKUtils.getGlideRequestOptions())
                .into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public List<SharedDataModel> getSelectedList() {
        return mSelectedList;
    }

    public static class SharedItemViewHolder extends RecyclerView.ViewHolder {

        CKCustomTextView appNameTv, titleTv, timeTv, urlTv;
        ImageView imageView;
        MaterialCheckBox checkBox;

        public SharedItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.picImg);
            appNameTv = itemView.findViewById(R.id.appNameTv);
            titleTv = itemView.findViewById(R.id.titleTv);
            timeTv = itemView.findViewById(R.id.dateTv);
            urlTv = itemView.findViewById(R.id.urlTv);
            checkBox = itemView.findViewById(R.id.checkbox);
        }
    }

    public void selectAll() {
        mSelectedList.clear();
        mSelectedList.addAll(mDataList);
        for (SharedDataModel model : mSelectedList) {
            model.setSelected(true);
        }
        notifyDataSetChanged();
    }

    public void clearAll() {
        //making checkbox changes
        for (SharedDataModel model : mSelectedList) {
            model.setSelected(false);
        }
        mSelectedList.clear();
        notifyDataSetChanged();
    }

    public void deleteSelectedItems() {
        mDataList.removeAll(getSelectedList());
        notifyDataSetChanged();
    }
}
