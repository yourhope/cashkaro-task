package jsk.cashkaro.framework.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import jsk.cashkaro.task.R;

/**
 * Created by Sathish on 5/25/2021
 */
public class CKShimmerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    int mCount = 0;

    public CKShimmerAdapter(Context context, int count) {
        mContext = context;
        mCount = count;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.shimmer_layout, parent, false);
        return new ShimmerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mCount;
    }

    public static class ShimmerViewHolder extends RecyclerView.ViewHolder {

        TextView tv_feed_main_title, tv_feed_main_desc;
        ImageView img_feed_main;

        public ShimmerViewHolder(@NonNull View itemView) {
            super(itemView);
            img_feed_main = itemView.findViewById(R.id.img_feed_main);
            tv_feed_main_title = itemView.findViewById(R.id.tv_feed_main_title);
            tv_feed_main_desc = itemView.findViewById(R.id.tv_feed_main_desc);
        }
    }
}
