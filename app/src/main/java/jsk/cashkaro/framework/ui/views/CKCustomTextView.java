package jsk.cashkaro.framework.ui.views;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by Sathish on 5/17/2020
 */
public class CKCustomTextView extends androidx.appcompat.widget.AppCompatTextView {

    private Typeface mTypeface;

    public CKCustomTextView(Context context) {
        super(context);
        initTextView();
    }

    public CKCustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTextView();
    }

    public CKCustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTextView();
    }

    private void initTextView() {
        Typeface mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/trebuchet-ms.ttf");
        setTypeface(mTypeface);
    }

    /**
     * set font
     * @param path : should be available in asset
     */
    public void setTypeface(String path) {
        if (TextUtils.isEmpty(path)) {
            try {
                mTypeface = Typeface.createFromAsset(getContext().getAssets(), path);
                this.setTypeface(mTypeface);
            } catch (Exception e) {
                Log.e("Font", "font not set");
            }
        }
    }
}
