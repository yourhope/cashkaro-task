package jsk.cashkaro.framework.ui.listener;

/**
 * Created by Sathish on 5/25/2021
 */
public interface SelectionListener {
    void isSelected();
}
