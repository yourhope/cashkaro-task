package jsk.cashkaro.framework.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.ui.listener.CKRecyclerViewClickListener;
import jsk.cashkaro.framework.ui.model.TaskDataItem;
import jsk.cashkaro.framework.ui.utils.CKUtils;
import jsk.cashkaro.framework.ui.views.CKCustomTextView;
import jsk.cashkaro.task.R;

/**
 * Created by Sathish on 5/22/2021
 */
public class CKRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private final Context mContext;
    private final List<TaskDataItem> mDataList;
    CKRecyclerViewClickListener mRecyclerViewClickListener;
    /* List Types*/
    public static final int JS_SINGLE_ITEM = 1;
    public static final int JS_TEXT_WITH_RIGHT_ICON = 2;
    public static final int JS_TEXT_WITH_LEFT_ICON = 3;
    public static final int JS_PROFILE_ITEM = 4;
    public static final int JS_PROFILE_INBOX_ITEM = 5;

    public CKRecyclerAdapter(Context context, List<TaskDataItem> dataList) {
        mContext = context;
        mDataList = dataList;
    }

    public CKRecyclerAdapter(Context context, List<TaskDataItem> dataList, CKRecyclerViewClickListener recyclerViewClickListener) {
        mContext = context;
        mDataList = dataList;
        mRecyclerViewClickListener = recyclerViewClickListener;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        // check here the viewType and return RecyclerView.ViewHolder based on view type
        switch (viewType) {
            case JS_SINGLE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.single_item, parent, false);
                return new SingleItemViewHolder(view);
            case JS_TEXT_WITH_LEFT_ICON:
                view = LayoutInflater.from(mContext).inflate(R.layout.text_with_left_icon, parent, false);
                return new SingleItemViewHolder(view);
            case JS_TEXT_WITH_RIGHT_ICON:
                view = LayoutInflater.from(mContext).inflate(R.layout.text_with_right_icon, parent, false);
                return new SingleItemViewHolder(view);
            case JS_PROFILE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.profile_item_layout, parent, false);
                return new ProfileItemViewHolder(view);
            case JS_PROFILE_INBOX_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.profile_inbox_item, parent, false);
                return new ProfileInboxItemViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final int itemType = mDataList.get(position).getViewType();
        TaskDataItem itemModel = mDataList.get(position);
        switch (itemType) {
            case JS_TEXT_WITH_RIGHT_ICON:
            case JS_TEXT_WITH_LEFT_ICON:
            case JS_SINGLE_ITEM:
                SingleItemViewHolder sViewHolder = (SingleItemViewHolder) holder;
                sViewHolder.tvName.setText(itemModel.getName());
                if(!TextUtils.isEmpty(itemModel.getImgUrl())){
                    Glide.with(mContext).load(itemModel.getImgUrl())
                            .apply(CKUtils.getGlideRequestOptions())
                            .into(sViewHolder.imageView);
                }
                setItemViewClickListener(holder, itemModel, position);
                break;
            case JS_PROFILE_ITEM:
                ProfileItemViewHolder pItemViewHolder = (ProfileItemViewHolder) holder;
                //pItemViewHolder.imageView.setImageResource(R.mipmap.ic_cash_karo);
                pItemViewHolder.ageWithHeightTv.setText(itemModel.getAge());
                pItemViewHolder.casteWithPlaceTv.setText(itemModel.getMessage());
                pItemViewHolder.lastLoginTv.setText(itemModel.getLastLogin());
                pItemViewHolder.nameWithIdTv.setText(itemModel.getName());
                setItemViewClickListener(holder, itemModel, position);

                //Logger.i("URL : " + itemModel.getImgUrl());
                Glide.with(mContext).load(itemModel.getImgUrl())
                        .apply(CKUtils.getGlideRequestOptions())
                        .into(pItemViewHolder.imageView);
                break;
            case JS_PROFILE_INBOX_ITEM:
                ProfileInboxItemViewHolder pInboxViewHolder = (ProfileInboxItemViewHolder) holder;
                //pInboxViewHolder.imageView.setImageResource(R.mipmap.ic_cash_karo);
                pInboxViewHolder.messageTv.setText(itemModel.getMessage());
                pInboxViewHolder.dateTv.setText(itemModel.getDate());
                pInboxViewHolder.nameTv.setText(itemModel.getName());
                setItemViewClickListener(holder, itemModel, position);

                //Logger.i("URL : " + itemModel.getImgUrl());
                Glide.with(mContext).load(itemModel.getImgUrl())
                        .apply(CKUtils.getGlideRequestOptions())
                        .into(pInboxViewHolder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int lViewType = mDataList.get(position).getViewType();
        switch (lViewType) {
            case JS_TEXT_WITH_RIGHT_ICON:
                return JS_TEXT_WITH_RIGHT_ICON;
            case JS_TEXT_WITH_LEFT_ICON:
                return JS_TEXT_WITH_LEFT_ICON;
            case JS_PROFILE_ITEM:
                return JS_PROFILE_ITEM;
            case JS_PROFILE_INBOX_ITEM:
                return JS_PROFILE_INBOX_ITEM;
            default:
                return JS_SINGLE_ITEM;
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class SingleItemViewHolder extends RecyclerView.ViewHolder {

        CKCustomTextView tvName;
        ImageView imageView;

        public SingleItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textView);
            imageView = itemView.findViewById(R.id.icon);
        }
    }

    public static class ProfileItemViewHolder extends RecyclerView.ViewHolder {

        CKCustomTextView nameWithIdTv, ageWithHeightTv, casteWithPlaceTv, lastLoginTv;
        ImageView imageView;

        public ProfileItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.picImg);
            nameWithIdTv = itemView.findViewById(R.id.nameWithIdTv);
            ageWithHeightTv = itemView.findViewById(R.id.ageWithHeightTv);
            casteWithPlaceTv = itemView.findViewById(R.id.casteWithPlaceTv);
            lastLoginTv = itemView.findViewById(R.id.lastLoginTv);
        }
    }

    public static class ProfileInboxItemViewHolder extends RecyclerView.ViewHolder {

        CKCustomTextView nameTv, messageTv, dateTv;
        ImageView imageView;

        public ProfileInboxItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.picImg);
            nameTv = itemView.findViewById(R.id.nameTv);
            messageTv = itemView.findViewById(R.id.messageTv);
            dateTv = itemView.findViewById(R.id.dateTv);
        }
    }

    /**
     * setting up the callback
     *
     * @param viewHolder   :
     * @param taskDataItem :
     * @param position     :
     */
    private void setItemViewClickListener(final RecyclerView.ViewHolder viewHolder, final TaskDataItem taskDataItem, final int position) {
        viewHolder.itemView.setOnClickListener(v -> {
            if (mRecyclerViewClickListener != null) {
                mRecyclerViewClickListener.onCellClickListener(viewHolder, viewHolder.itemView, position, taskDataItem);
            }
        });
    }
}