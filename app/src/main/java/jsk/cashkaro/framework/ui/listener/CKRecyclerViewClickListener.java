package jsk.cashkaro.framework.ui.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import jsk.cashkaro.framework.ui.model.TaskDataItem;

/**
 * Created by Sathish on 5/24/2021
 */
public interface CKRecyclerViewClickListener {

    void onCellClickListener(RecyclerView.ViewHolder viewHolder, View view, int position, TaskDataItem dataItem);
}

