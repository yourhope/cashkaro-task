package jsk.cashkaro.framework.ui.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jsk.cashkaro.task.R;

/**
 * Created by Sathish on 5/23/2021
 */
public class CKUtils {

    public static void setRecyclerLayoutManager(Context context, RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
    }

    public static String getDateAndTime() {
        Date lDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.getDefault());
        return format.format(lDate);
    }

    public static String getAppName(Context aContext, String aPackageName) {
        PackageManager packageManager = aContext.getApplicationContext().getPackageManager();
        String appName = "<unknown>";
        try {
            appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(aPackageName, PackageManager.GET_META_DATA));
        } catch (Exception e) {
        }
        return appName;
    }

    public static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static RequestOptions getGlideRequestOptions(){
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_cash_karo)
                .error(R.mipmap.ic_cash_karo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .priority(Priority.HIGH);

        return requestOptions;
    }
}
