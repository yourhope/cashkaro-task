package jsk.cashkaro.framework.session.service;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.POST;

/**
 * Created by Sathish on 5/22/2021
 */
public interface SessionService {
    //TODO : auto extension

    @POST("/logout")
    Call<Void> logout(@FieldMap Map<String, String> logoutMap);

}
