package jsk.cashkaro.framework.session.base;

/**
 * Created by Sathish on 5/22/2021
 */
public interface IUserProfile {

    void setItem(String key, Object value);
    void resetProfile();
    void deleteItem(String key);
    void setProfileType(String profileType);
    Object getItem(String key);
    String getProfileType();
}
