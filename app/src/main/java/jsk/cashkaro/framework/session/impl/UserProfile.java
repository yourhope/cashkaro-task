package jsk.cashkaro.framework.session.impl;

import java.util.HashMap;

import jsk.cashkaro.framework.common.utils.CommonUtils;
import jsk.cashkaro.framework.session.base.IUserProfile;


/**
 * Created by Sathish on 5/22/2021
 */
public class UserProfile implements IUserProfile {

    HashMap<String, Object> mProfileMap = new HashMap<>();
    private String mProfileType;

    public UserProfile(String profileType) {
        this.mProfileType = profileType;
    }

    @Override
    public void setItem(String key, Object value) {
        if (CommonUtils.isNullOrEmpty(key) && CommonUtils.isNotNull(value)) {
            this.mProfileMap.put(key, value);
        }
    }

    @Override
    public void resetProfile() {
        mProfileMap.clear();
        this.setProfileType("VISITOR");
    }

    @Override
    public void deleteItem(String key) {
        mProfileMap.remove(key);
    }

    @Override
    public void setProfileType(String profileType) {
        this.mProfileType = profileType;
    }

    @Override
    public Object getItem(String key) {
        return !CommonUtils.isNullOrEmpty(key) ? this.mProfileMap.get(key) : null;
    }

    @Override
    public String getProfileType() {
        return this.mProfileType;
    }
}
