package jsk.cashkaro.framework.session.di;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.framework.session.base.IUserProfile;
import jsk.cashkaro.framework.session.impl.UserProfile;
import jsk.cashkaro.framework.session.service.SessionService;
import retrofit2.Retrofit;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public class SessionModule {
    public SessionModule() {
    }
    @Provides
    static IUserProfile provideUserProfile(){
        return new UserProfile("VISITOR");
    }

    @Singleton
    @Provides
    SessionService provideInitService(@Named("INIT_RETROFIT")Retrofit retrofit){
        return (SessionService)retrofit.create(SessionService.class);
    }
}
