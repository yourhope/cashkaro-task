package jsk.cashkaro.framework.locale;

/**
 * Created by Sathish on 5/22/2021
 */
public interface AbstractLocale {
    String getLocale();

    void setLocale(String locale);
}
