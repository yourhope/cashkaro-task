package jsk.cashkaro.framework.common.utils.rx;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.atomic.AtomicBoolean;

import jsk.cashkaro.framework.common.utils.logger.Logger;


/**
 * Created by Sathish on 5/22/2021
 */
public class SingleLiveEvent<T> extends MutableLiveData<T> {
    private static final String TAG = "SingleLiveEvent";
    private final AtomicBoolean mPending = new AtomicBoolean(false);

    @MainThread
    public void observe(LifecycleOwner lifecycleOwner, final Observer<? super T> observer){
        if(this.hasActiveObservers()){
            Logger.w(TAG, "mutiple observers registered but only one will be notified of changes ");
        }

        super.observe(lifecycleOwner, new Observer<T>() {
            @Override
            public void onChanged(T t) {
                if(SingleLiveEvent.this.mPending.compareAndSet(true,false));
                observer.onChanged(t);
            }
        });
    }

    @MainThread
    public void setValue(@Nullable T t){
        this.mPending.set(true);
        super.setValue(t);
    }

    @MainThread
    public void call(){
        this.setValue((T) null);
    }
}
