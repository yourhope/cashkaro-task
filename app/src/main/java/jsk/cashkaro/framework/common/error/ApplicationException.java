package jsk.cashkaro.framework.common.error;

import java.util.List;
import java.util.Map;

/**
 * Created by Sathish on 5/22/2021
 */
public class ApplicationException extends BaseException {
    private Map<String, String> detailMap;

    public ApplicationException(String mContent, String mCode, String mTitle, Error.DisplayType mDisplayType, List<Error.ActionHandler> handlers) {
        super(mContent, mCode, mTitle, mDisplayType,handlers);
    }

    public ApplicationException(String mContent, String mCode, String mTitle, Error.DisplayType displayType) {
        super(mContent, mCode, mTitle,displayType);
    }

    public ApplicationException(String mContent, String mCode, String mTitle) {
        super(mContent, mCode, mTitle);
    }

    public ApplicationException(String code) {
       this.mCode = code;
    }
    public ApplicationException(String code, Throwable throwable) {
        this.mCode = code;
        this.setException(throwable);
    }

    public void setDetailMap(Map<String, String> detailMap){
        this.detailMap = detailMap;
    }

    public Map<String, String> getDetailMap() {
        return detailMap;
    }
}
