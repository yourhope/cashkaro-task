package jsk.cashkaro.framework.common.utils.rx;

import java.util.Stack;

/**
 * Created by Sathish on 5/22/2021
 */
public class CustomDisposableObserverFactory {
    private static CustomDisposableObserverFactory instance;
    private Stack<CustomDisposableObserver> mStack = new Stack<>();

    public CustomDisposableObserverFactory() {
    }

    public static CustomDisposableObserverFactory getInstance() {
        if (instance == null) {
            instance = new CustomDisposableObserverFactory();
        }
        return instance;
    }

    public <T> CustomDisposableObserver<T> getCachedObserver() {
        return (CustomDisposableObserver) this.mStack.peek();
    }

    public Stack<CustomDisposableObserver> getStack() {
        return this.mStack;
    }
}
