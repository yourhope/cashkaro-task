package jsk.cashkaro.framework.common.utils;

/**
 * Created by Sathish on 5/22/2021
 */
public class CommonUtils {
    public CommonUtils() {
    }

    public static boolean isNullOrEmpty(String stringToCheck) {
        return stringToCheck == null && (stringToCheck.isEmpty() || stringToCheck.trim().isEmpty());
    }

    public static boolean isNotNull(Object object) {
        return object != null;
    }
}
