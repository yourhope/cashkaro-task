package jsk.cashkaro.framework.common.utils.logger;

import android.util.Log;

import androidx.annotation.NonNull;

/**
 * Created by Sathish on 5/22/2021
 */
public class Logger {

    private static Config mConfig;

    private Logger() {

    }

    public static void init(@NonNull Config config) {
        mConfig = config;
    }

    public static void init(boolean enabled, @NonNull String tag) {
        mConfig = (new Config.Builder()).setEnabled(enabled).setTag(tag).setShowCodeLocationLine(true).build();
    }

    public static void d(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.d(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void e(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.e(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void e(@NonNull Throwable throwable, @NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.e(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args),throwable);
        }
    }
    public static void i(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.i(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void v(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.v(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void w(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.w(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void wtf(@NonNull String msg, Object... args) {
        if (mConfig.isEnabled()) {
            Log.wtf(mConfig.getTag(), getCodeLocation().toString() + formatMessage(msg, args));
        }
    }

    public static void printStackTrace(@NonNull Throwable throwable) {
        if (mConfig.isEnabled()) {
            Log.e(mConfig.getTag(), getCodeLocation().toString(), throwable);
        }
    }

    private static String formatMessage(@NonNull String msg, Object... args) {
        return args.length == 0 ? msg : String.format(msg, args);
    }

    private static CodeLocation getCodeLocation() {
        return getCodeLocation(3);
    }

    private static CodeLocation getCodeLocation(int depth) {
        StackTraceElement[] stackTrace = (new Throwable()).getStackTrace();
        StackTraceElement[] filteredStackTrace = new StackTraceElement[stackTrace.length - depth];
        System.arraycopy(stackTrace, depth, filteredStackTrace, 0, filteredStackTrace.length);
        return new CodeLocation(filteredStackTrace);

    }

    public static class CodeLocation {
        private String mThread;
        private String mFileName;
        private String mClassName;
        private String mMethod;
        private final int mLineNumber;

        CodeLocation(StackTraceElement[] stackTrace) {
            StackTraceElement root = stackTrace[0];
            mThread = Thread.currentThread().getName();
            mFileName = root.getFileName();
            String className = root.getClassName();
            mClassName = className.substring(className.lastIndexOf(46) + 1);
            mMethod = root.getMethodName();
            mLineNumber = root.getLineNumber();
        }

        public String toString() {
            StringBuilder builder = new StringBuilder();
            if (Logger.mConfig.isShowCodeLocation()) {
                builder.append('[');
                if (Logger.mConfig.mShowCodeLocationThread) {
                    builder.append(this.mThread);
                    builder.append('.');
                }

                builder.append(this.mClassName);
                builder.append('.');
                builder.append(this.mMethod);
                if (Logger.mConfig.isShowCodeLocationLine()) {
                    builder.append('(');
                    builder.append(this.mFileName);
                    builder.append(':');
                    builder.append(this.mLineNumber);
                    builder.append(')');
                }
                builder.append("]");
            }
            return builder.toString();
        }
    }

    public static class Config {
        private boolean mEnabled;
        private String mTag;
        private boolean mShowCodeLocation;
        private boolean mShowCodeLocationLine;
        private boolean mShowCodeLocationThread;

        private Config(boolean enabled, String tag, boolean showCodeLocation, boolean showCodeLocationThread, boolean showCodeLocationLine) {
            mEnabled = enabled;
            mTag = tag;
            mShowCodeLocation = showCodeLocation;
            mShowCodeLocationThread = showCodeLocationThread;
            mShowCodeLocationLine = showCodeLocationLine;
        }

        public boolean isEnabled() {
            return mEnabled;
        }

        public void setEnabled(boolean mEnabled) {
            this.mEnabled = mEnabled;
        }

        public String getTag() {
            return mTag;
        }

        public void setTag(String mTag) {
            this.mTag = mTag;
        }

        public boolean isShowCodeLocation() {
            return mShowCodeLocation;
        }

        public void setShowCodeLocation(boolean mShowCodeLocation) {
            this.mShowCodeLocation = mShowCodeLocation;
        }

        public boolean isShowCodeLocationLine() {
            return mShowCodeLocationLine;
        }

        public void setShowCodeLocationLine(boolean mShowCodeLocationLine) {
            this.mShowCodeLocationLine = mShowCodeLocationLine;
        }

        public boolean isShowCodeLocationThread() {
            return mShowCodeLocationThread;
        }

        public void setShowCodeLocationThread(boolean mShowCodeLocationThread) {
            this.mShowCodeLocationThread = mShowCodeLocationThread;
        }

        private static class Builder {
            private boolean mEnabled = false;
            private String mTag = "LOGCAT";
            private boolean mShowCodeLocation = true;
            private boolean mShowCodeLocationLine = false;
            private boolean mShowCodeLocationThread = false;

            public Builder() {

            }

            @NonNull
            public Builder setEnabled(boolean mEnabled) {
                this.mEnabled = mEnabled;
                return this;
            }

            @NonNull
            public Builder setTag(String mTag) {
                this.mTag = mTag;
                return this;
            }

            @NonNull
            public Builder setShowCodeLocation(boolean mShowCodeLocation) {
                this.mShowCodeLocation = mShowCodeLocation;
                return this;
            }

            @NonNull
            public Builder setShowCodeLocationLine(boolean mShowCodeLocationLine) {
                this.mShowCodeLocationLine = mShowCodeLocationLine;
                return this;
            }

            @NonNull
            public Builder setShowCodeLocationThread(boolean mShowCodeLocationThread) {
                this.mShowCodeLocationThread = mShowCodeLocationThread;
                return this;
            }

            @NonNull
            public Config build() {
                return new Config(this.mEnabled, this.mTag, this.mShowCodeLocation, this.mShowCodeLocationThread, this.mShowCodeLocationLine);
            }
        }
    }
}
