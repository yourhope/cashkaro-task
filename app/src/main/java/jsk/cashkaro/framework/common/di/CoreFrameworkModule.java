package jsk.cashkaro.framework.common.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.framework.common.error.ServiceExceptionHandler;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.common.utils.rx.RxSchedulerProvider;
import jsk.cashkaro.framework.network.di.NetworkModule;
import jsk.cashkaro.framework.session.di.SessionModule;

/**
 * Created by Sathish on 5/22/2021
 */

@Module(includes = {SessionModule.class, NetworkModule.class})
public class CoreFrameworkModule {
    public CoreFrameworkModule() {
    }

    @Provides
    @Singleton
    RxSchedulerProvider provideRxSchedulerProvider() {
        return new RxSchedulerProvider();
    }

    @Provides
    RxEventBus provideRxEventBus() {
        return RxEventBus.getInstance();
    }

    @Singleton
    @Provides
    ServiceExceptionHandler provideExceptionHandler() {
        return new ServiceExceptionHandler();
    }
}
