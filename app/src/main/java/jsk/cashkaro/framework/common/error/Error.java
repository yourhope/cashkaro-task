package jsk.cashkaro.framework.common.error;

/**
 * Created by Sathish on 5/22/2021
 */
public class Error {
    public Error() {

    }

    public static class ErrorCode {
        public static final String CODE_CONNECT_EXCEPTION = "ERROR_001_CODE_CONNECT_EXCEPTION";
        public static final String CODE_SOCKET_EXCEPTION = "ERROR_002_CODE_SOCKET_EXCEPTION";
        public static final String CODE_UNKNOWN_HOST_EXCEPTION = "ERROR_003_CODE_UNKNOWN_HOST_EXCEPTION";
        public static final String CODE_SOCKET_TIMEOUT_EXCEPTION = "ERROR_004_CODE_SOCKET_TIMEOUT_EXCEPTION";
        public static final String CODE_MALFORMED_JSON_EXCEPTION = "ERROR_005_CODE_MALFORMED_JSON_EXCEPTION";
        public static final String CODE_NO_NETWORK_EXCEPTION = "ERROR_006_CODE_NO_NETWORK_EXCEPTION";
        public static final String CODE_SSL_EXCEPTION = "ERROR_007_CODE_SSL_EXCEPTION";
        public static final String CODE_CERT_PINNING_EXCEPTION = "ERROR_008_CODE_SSL_EXCEPTION";

        public static final String CODE_GENERAL_EXCEPTION = "ERROR_100_CODE_GENERAL_EXCEPTION";

        public static final String CODE_INVALID_REQUEST_EXCEPTION = "ERROR_400";
        public static final String CODE_ACCESS_NOT_CONFIGURED_EXCEPTION = "ERROR_403";
        public static final String CODE_RESOURCE_NOT_FOUND_EXCEPTION = "ERROR_404";
        public static final String CODE_SERVER_UNAVAILABLE_EXCEPTION = "ERROR_500";
        public static final String CODE_NO_CONTENT_EXCEPTION = "ERROR_204";
        public static final String CODE_NO_RADATA_EXCEPTION = "ERROR_200";

        public ErrorCode() {
        }
    }

    public static class ActionHandler {
        String mLabel;
        Runnable mAction;

        public ActionHandler(String mLabel, Runnable mAction) {
            this.mLabel = mLabel;
            this.mAction = mAction;
        }
    }

    public static enum ErrorButtonNavigationType {
        NOTHING,
        DASHBOARD,
        LOGIN,
        SESSION_ENDED;

        private ErrorButtonNavigationType() {

        }

    }

    public static enum DisplayType {
        SCREEN,
        DIALOG;

        private DisplayType() {

        }
    }
}
