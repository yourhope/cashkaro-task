package jsk.cashkaro.framework.common.error;

import android.util.MalformedJsonException;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import retrofit2.Response;

/**
 * Created by Sathish on 5/22/2021
 */
public class ServiceExceptionHandler {

    public ServiceExceptionHandler() {

    }

    public BaseException mapException(Throwable exception) {
        if (exception instanceof BaseException) {
            return (BaseException) exception;
        } else if (exception instanceof SSLPeerUnverifiedException) {
            return new ApplicationException(Error.ErrorCode.CODE_CERT_PINNING_EXCEPTION, exception);
        } else if (exception instanceof ConnectException) {
            return new ApplicationException(Error.ErrorCode.CODE_CONNECT_EXCEPTION, exception);
        } else if (exception instanceof SocketException) {
            return new ApplicationException(Error.ErrorCode.CODE_SOCKET_EXCEPTION, exception);
        } else if (exception instanceof UnknownHostException) {
            return new ApplicationException(Error.ErrorCode.CODE_UNKNOWN_HOST_EXCEPTION, exception);
        } else if (exception instanceof SocketTimeoutException) {
            return new ApplicationException(Error.ErrorCode.CODE_SOCKET_TIMEOUT_EXCEPTION, exception);
        } else if (exception instanceof MalformedJsonException) {
            return new ApplicationException(Error.ErrorCode.CODE_MALFORMED_JSON_EXCEPTION, exception);
        } else if(exception != null){
            return exception instanceof SSLException ? new ApplicationException(Error.ErrorCode.CODE_SSL_EXCEPTION, exception) : new ApplicationException(Error.ErrorCode.CODE_GENERAL_EXCEPTION, exception);
        }else {
            Logger.e("null in error mapping");
            return null;
        }
    }

    public BaseException mapApiException(Response response) {
        if (response != null) {
            switch (response.code()) {
                case 400:
                    return new ApplicationException(Error.ErrorCode.CODE_INVALID_REQUEST_EXCEPTION);
                case 403:
                    return new ApplicationException(Error.ErrorCode.CODE_ACCESS_NOT_CONFIGURED_EXCEPTION);
                case 404:
                    return new ApplicationException(Error.ErrorCode.CODE_RESOURCE_NOT_FOUND_EXCEPTION);
                case 500:
                    return new ApplicationException(Error.ErrorCode.CODE_SERVER_UNAVAILABLE_EXCEPTION);
                default:
                    return new ApplicationException(String.valueOf(response.code()));
            }
        }
        return new BaseException();
    }

    public static boolean isNetworkException(BaseException exception) {
        return exception.getCode().equalsIgnoreCase(Error.ErrorCode.CODE_NO_NETWORK_EXCEPTION) ||
                exception.getCode().equalsIgnoreCase(Error.ErrorCode.CODE_SOCKET_EXCEPTION) ||
                exception.getCode().equalsIgnoreCase(Error.ErrorCode.CODE_UNKNOWN_HOST_EXCEPTION) ||
                exception.getCode().equalsIgnoreCase(Error.ErrorCode.CODE_CONNECT_EXCEPTION);
    }
}

