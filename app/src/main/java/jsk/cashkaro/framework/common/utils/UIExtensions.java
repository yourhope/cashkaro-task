package jsk.cashkaro.framework.common.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Sathish on 5/22/2021
 */
public class UIExtensions {
    public UIExtensions() {
    }

    public static void showActionDialog(Context context, String title, String message, String positiveButtonText, final Runnable positiveAction, String negatieButtonText, Runnable negatieAction, boolean cancelable) {
        AlertDialog.Builder alertBuilder = (new AlertDialog.Builder(context))
                .setCancelable(cancelable)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, (dialog, which) -> {
                    positiveAction.run();
                });
        if (negatieButtonText != null) {
            alertBuilder.setNegativeButton(negatieButtonText, ((dialog, which) -> {
                negatieAction.run();
            }));
        }
    }

    public static void showSingleActionDialog(Context context, String title, String message, String positiveButtonText, final Runnable positiveAction, boolean cancelable) {
        showActionDialog(context, title, message, positiveButtonText, positiveAction, (String) null, (Runnable) null, cancelable);
    }

    public static ProgressDialog showProgressDialog(Context context, int msgId) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(msgId));
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void hideProgressDialog(ProgressDialog progressDialog) {
        progressDialog.dismiss();
    }
}
