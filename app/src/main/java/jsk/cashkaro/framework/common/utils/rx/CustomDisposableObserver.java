package jsk.cashkaro.framework.common.utils.rx;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.internal.disposables.DisposableHelper;

/**
 * Created by Sathish on 5/22/2021
 */
public class CustomDisposableObserver<T> implements Observer<T>, Disposable {

    final AtomicReference<Disposable> atomicReference = new AtomicReference<>();
    CustomDisposableObserverFactory mFactory = CustomDisposableObserverFactory.getInstance();
    private Consumer<T> mNextAction;
    private Consumer<Throwable> mErrorAction;
    private Action mCompleteAction;
    private boolean mShouldCache;

    public CustomDisposableObserver(Consumer<T> nextAction, Consumer<Throwable> errorAction, Action completeAction, boolean shouldCache) {
        this.mNextAction = nextAction;
        this.mErrorAction = errorAction;
        this.mCompleteAction = completeAction;
        this.mShouldCache = shouldCache;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static <T> CustomDisposableObserver<T> from(Consumer<T> nextAction, Consumer<Throwable> errorAction, Action completeAction) {
        return new CustomDisposableObserver(nextAction, errorAction, completeAction, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static <T> CustomDisposableObserver<T> from() {
        return from((Consumer) null, (Consumer) null, (Action) null);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static <T> CustomDisposableObserver<T> from(Consumer<T> nextAction) {
        return from(nextAction, (Consumer) null, (Action) null);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static <T> CustomDisposableObserver<T> from(Consumer<T> nextAction, Consumer<Throwable> errorAction) {
        return from(nextAction, errorAction, (Action) null);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static <T> CustomDisposableObserver<T> from(Consumer<T> nextAction, Consumer<Throwable> errorAction, boolean shouldCache) {
        return new CustomDisposableObserver(nextAction, errorAction, (Action) null, shouldCache);
    }

    private void onStart() {

    }

    @Override
    public void onSubscribe(Disposable d) {
        if (DisposableHelper.setOnce(this.atomicReference, d)) {
            this.onStart();
        }
        synchronized (this) {
            if (mShouldCache) {
                this.mFactory.getStack().push(this);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onNext(T t) {
        if (this.mNextAction != null) {
            try {
                this.mNextAction.accept(t);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onError(Throwable e) {
        if (this.mErrorAction != null) {
            try {
                this.mErrorAction.accept(e);
            } catch (Exception error) {
                error.printStackTrace();
            }
        }
    }

    @Override
    public void onComplete() {
        if (this.mCompleteAction != null) {
            try {
                this.mCompleteAction.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void dispose() {
        synchronized (this) {
            if (this.mShouldCache && mFactory.getStack() != null && this.mFactory.getStack().contains(this)) {
                this.mFactory.getStack().remove(this);
            }
        }
    }

    @Override
    public boolean isDisposed() {
        return this.atomicReference.get() == DisposableHelper.DISPOSED;
    }
}
