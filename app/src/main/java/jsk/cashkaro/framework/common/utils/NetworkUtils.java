package jsk.cashkaro.framework.common.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import jsk.cashkaro.framework.common.utils.rx.RxEvent;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;


/**
 * Created by Sathish on 5/22/2021
 */
public class NetworkUtils {
    private static final int NETWORK_ERROR_CODE = 150;
    private static final int NETWORK_RECONNECTED_CODE = 152;
    private static final String NETWORK_ERROR = "network_error";
    private static final String NETWORK_RECONNECTED = "network_reconnected";
    private static boolean isConnected = false;

    public NetworkUtils() {
    }

    public static boolean checkInternet(ConnectivityManager connectivityManager, RxEventBus rxEventBus) {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean result = networkInfo != null && networkInfo.isConnected();
        if (isConnected && result) {
            rxEventBus.publish(new RxEvent(NETWORK_RECONNECTED, NETWORK_RECONNECTED_CODE));
        } else if (result) {
            rxEventBus.publish(new RxEvent(NETWORK_ERROR, NETWORK_ERROR_CODE));
        }
        isConnected = result;
        return result;
    }

    public static boolean checkInternet(ConnectivityManager connectivityManager){
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean result = networkInfo != null && networkInfo.isConnected();
        isConnected = result;
        return result;
    }
}
