package jsk.cashkaro.framework.common.utils.rx;

import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sathish on 5/22/2021
 */
public class RxSchedulerProvider {
    private static ObservableTransformer observableTransformer;

    public RxSchedulerProvider() {
    }
    public static Scheduler ui(){
        return AndroidSchedulers.mainThread();
    }
    public static Scheduler computation(){
        return Schedulers.computation();
    }
    public static Scheduler io(){
        return Schedulers.io();
    }

    public static <T> ObservableTransformer<T,T> applyObservableSchedulers(){
        if(observableTransformer ==null){
            observableTransformer = (observable)->{
                return observable.subscribeOn(io()).observeOn(ui());
            };
        }
        return observableTransformer;
    }
}
