package jsk.cashkaro.framework.common.utils.rx;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Sathish on 5/22/2021
 */
public class RxEventBus {
    private static RxEventBus mInstance;
    private PublishSubject<RxEvent> bus = PublishSubject.create();

    private RxEventBus() {

    }

    public static RxEventBus getInstance() {
        if (mInstance == null) {
            mInstance = new RxEventBus();
        }
        return mInstance;
    }
    public void publish(RxEvent event){
        this.bus.onNext(event);
    }

    public Observable<RxEvent> listenTo(Class<RxEvent> rxEventClass){
        return this.bus.ofType(rxEventClass);
    }

    public boolean hasObservers(){
        return this.bus.hasObservers();
    }
}
