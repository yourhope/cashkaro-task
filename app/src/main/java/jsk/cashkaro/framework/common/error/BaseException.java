package jsk.cashkaro.framework.common.error;

import org.json.JSONObject;

import java.util.List;

import jsk.cashkaro.framework.common.utils.logger.Logger;


/**
 * Created by Sathish on 5/22/2021
 */
public class BaseException extends RuntimeException {
    private String mContent;
    public String mCode;
    private String mTitle;
    private Throwable mException;
    private Error.DisplayType mDisplayType;
    private List<Error.ActionHandler> mHandlers;
    private JSONObject mProxyErrorJSON;

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String mCode, String mTitle, Throwable mException, Error.DisplayType mDisplayType, List<Error.ActionHandler> mHandlers) {
        this.mDisplayType = Error.DisplayType.DIALOG;
        this.mCode = mCode;
        this.mTitle = mTitle;
        this.mException = mException;
        this.mDisplayType = mDisplayType;
        this.mHandlers = mHandlers;
    }

    public BaseException(String mContent, String mCode, String mTitle, Error.DisplayType mDisplayType) {
        this.mDisplayType = Error.DisplayType.DIALOG;
        this.mContent = mContent;
        this.mCode = mCode;
        this.mTitle = mTitle;
        this.mDisplayType = mDisplayType;
    }
    public BaseException(String mContent, String mCode, String mTitle, Error.DisplayType mDisplayType, List<Error.ActionHandler> handlers) {
        this.mDisplayType = Error.DisplayType.DIALOG;
        this.mContent = mContent;
        this.mCode = mCode;
        this.mTitle = mTitle;
        this.mDisplayType = mDisplayType;
        mHandlers = handlers;
    }

    public BaseException(String mContent, String mCode, String mTitle) {
        this.mDisplayType = Error.DisplayType.DIALOG;
        this.mContent = mContent;
        this.mCode = mCode;
        this.mTitle = mTitle;
    }

    BaseException(){
        this.mDisplayType = Error.DisplayType.DIALOG;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String mContent) {
        this.mContent = mContent;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Throwable getException() {
        return mException;
    }

    public void setException(Throwable mException) {
        this.mException = mException;
    }

    public Error.DisplayType getDisplayType() {
        return mDisplayType;
    }

    public void setDisplayType(Error.DisplayType mDisplayType) {
        this.mDisplayType = mDisplayType;
    }

    public List<Error.ActionHandler> getHandlers() {
        return mHandlers;
    }

    public void setHandlers(List<Error.ActionHandler> mHandlers) {
        this.mHandlers = mHandlers;
    }

    public JSONObject getProxyErrorJSON() {
        return mProxyErrorJSON;
    }

    public void setProxyErrorJSON(JSONObject mProxyErrorJSON) {
        this.mProxyErrorJSON = mProxyErrorJSON;
    }

    public String getessage(){
        return this.mException == null ? this.getCode() : this.mException.getMessage();
    }

    public void printStackTrace(){
        if(this.mException!=null){
            this.mException.printStackTrace();
        }else{
            Logger.e(this.getMessage());
        }
    }
}
