package jsk.cashkaro.framework.network.store;

/**
 * Created by Sathish on 5/22/2021
 */
public class CookieStore {

    private String cookie;
    private boolean isDirty;
    public static String SET_COOKIE = "Set-Cookie";
    public static String COOKIE = "Cookie";
    public static String LOCALE = "defaultLocale";

    public CookieStore(){

    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getCookie() {
        return cookie;
    }

    public void setDirty(boolean dirty) {
        isDirty = dirty;
    }

    public boolean isDirty() {
        return isDirty;
    }
}
