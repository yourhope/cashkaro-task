package jsk.cashkaro.framework.network.di;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;


import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.framework.common.error.ServiceExceptionHandler;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.controller.ServiceController;
import jsk.cashkaro.framework.network.controller.ServiceControllerImpl;
import jsk.cashkaro.framework.network.interceptor.ApiInterceptor;
import jsk.cashkaro.framework.network.interceptor.InitInterceptor;
import jsk.cashkaro.framework.network.interceptor.LogInterceptor;
import jsk.cashkaro.framework.network.store.CookieStore;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public class NetworkModule {
    private static final String BASE_URL = "BASE_URL";
    private static final String IS_DEBUG = "isDebug";
    private static final String SIMPLE_OKHTTP = "SIMPLE_CLIENT";
    private static final String INIT_OKHTTP = "INIT_CLIENT";
    private static final String API_OKHTTP = "API_CLIENT";
    private static final String LOG_OKHTTP = "LOG_CLIENT";
    private static final String SIMPLE_RETROFIT = "SIMPLE_RETROFIT";
    private static final String INIT_RETROFIT = "INIT_RETROFIT";
    private static final String API_RETROFIT = "API_RETROFIT";
    private static final String LOG_RETROFIT = "LOG_RETROFIT";

    public NetworkModule() {

    }

    @Singleton
    @Provides
    InitInterceptor provideInitInterceptor(CookieStore cookieStore, RxEventBus rxEventBus, AbstractLocale locale) {
        return new InitInterceptor(cookieStore, rxEventBus, locale);
    }

    @Singleton
    @Provides
    ApiInterceptor provideApiInterceptor(CookieStore cookieStore, RxEventBus rxEventBus, AbstractLocale locale) {
        return new ApiInterceptor(cookieStore, rxEventBus, locale);
    }

    @Singleton
    @Provides
    LogInterceptor provideLogInterceptor(CookieStore cookieStore, AbstractLocale locale) {
        return new LogInterceptor(cookieStore, locale);
    }

    @Singleton
    @Provides
    ServiceController provideServiceController(ConnectivityManager connectivityManager, ServiceExceptionHandler handler, RxEventBus rxEventBus) {
        return new ServiceControllerImpl(connectivityManager, handler, rxEventBus);
    }

    @Singleton
    @Provides
    ConnectivityManager provideConnectivityManager(Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor(@Named("isDebug") Boolean isDebug) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        if (isDebug) {
            //httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            //disabling logs
        } else {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return httpLoggingInterceptor;
    }

    @Singleton
    @Provides
    @Named(SIMPLE_OKHTTP)
    OkHttpClient provideSimpleOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return (new OkHttpClient.Builder()).connectTimeout(100L, TimeUnit.SECONDS).readTimeout(100L, TimeUnit.SECONDS).writeTimeout(100L, TimeUnit.SECONDS).addInterceptor(httpLoggingInterceptor).build();
    }

    @Singleton
    @Provides
    @Named(INIT_OKHTTP)
    OkHttpClient provideInitOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, InitInterceptor initInterceptor) {
        return (new OkHttpClient.Builder()).connectTimeout(100L, TimeUnit.SECONDS).readTimeout(100L, TimeUnit.SECONDS).writeTimeout(100L, TimeUnit.SECONDS).addInterceptor(initInterceptor).addInterceptor(httpLoggingInterceptor).build();
    }

    @Singleton
    @Provides
    @Named(API_OKHTTP)
    OkHttpClient provideApiOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, ApiInterceptor apiInterceptor) {
        return (new OkHttpClient.Builder()).connectTimeout(100L, TimeUnit.SECONDS).readTimeout(100L, TimeUnit.SECONDS).writeTimeout(100L, TimeUnit.SECONDS).addInterceptor(apiInterceptor).addInterceptor(httpLoggingInterceptor).build();
    }

    @Singleton
    @Provides
    @Named(LOG_OKHTTP)
    OkHttpClient provideLogOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, LogInterceptor logInterceptor) {
        return (new OkHttpClient.Builder()).connectTimeout(100L, TimeUnit.SECONDS).readTimeout(100L, TimeUnit.SECONDS).writeTimeout(100L, TimeUnit.SECONDS).addInterceptor(logInterceptor).addInterceptor(httpLoggingInterceptor).build();
    }

    @Singleton
    @Provides
    @Named(SIMPLE_RETROFIT)
    Retrofit provideSimpleRetrofit(@Named(SIMPLE_OKHTTP) OkHttpClient client, @Named(BASE_URL) String baseURL) {
        return (new Retrofit.Builder()).baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    @Singleton
    @Provides
    @Named(API_RETROFIT)
    Retrofit provideApiRetrofit(@Named(API_OKHTTP) OkHttpClient client, @Named(BASE_URL) String baseURL) {
        return (new Retrofit.Builder()).baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    @Singleton
    @Provides
    @Named(INIT_RETROFIT)
    Retrofit provideInitRetrofit(@Named(INIT_OKHTTP) OkHttpClient client, @Named(BASE_URL) String baseURL) {
        return (new Retrofit.Builder()).baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    @Singleton
    @Provides
    @Named(LOG_RETROFIT)
    Retrofit provideLogRetrofit(@Named(LOG_OKHTTP) OkHttpClient client, @Named(BASE_URL) String baseURL) {
        return (new Retrofit.Builder()).baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    @Singleton
    @Provides
    CookieStore provideCookieStore() {
        return new CookieStore();
    }
}
