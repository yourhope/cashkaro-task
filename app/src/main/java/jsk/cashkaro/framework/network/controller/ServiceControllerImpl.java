package jsk.cashkaro.framework.network.controller;

import android.net.ConnectivityManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.function.Function;

import io.reactivex.Observable;
import jsk.cashkaro.framework.common.error.ApplicationException;
import jsk.cashkaro.framework.common.error.Error;
import jsk.cashkaro.framework.common.error.ServiceExceptionHandler;
import jsk.cashkaro.framework.common.utils.NetworkUtils;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Sathish on 5/22/2021
 */
public class ServiceControllerImpl implements ServiceController {

    private RxEventBus mRxEventBus;
    private ConnectivityManager mConnectivityManager;
    private ServiceExceptionHandler mServiceExceptionHandler;
    private Function<String, Observable<?>> mReInitEligibility;

    public ServiceControllerImpl(ConnectivityManager connectivityManager, ServiceExceptionHandler serviceExceptionHandler, RxEventBus rxEventBus) {
        this.mConnectivityManager = connectivityManager;
        this.mServiceExceptionHandler = serviceExceptionHandler;
        this.mRxEventBus = rxEventBus;

        //init
        setReInitEligibility(screenName -> {
            return Observable.just(false);
        });
    }

    @Override
    public <T> Observable<T> executeInit(Call<T> call) {
        return Observable.just(NetworkUtils.checkInternet(this.mConnectivityManager, this.mRxEventBus)).flatMap((available) -> {
            if (available) {
                Logger.d("available");
                return this.exec(call, "Init", true);
            } else {
                Logger.e("network Not available>>>>>>>>>>>>>>>>>");
                return Observable.error(new ApplicationException(Error.ErrorCode.CODE_NO_NETWORK_EXCEPTION));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public <T> Observable<T> execute(Call<T> call, String screenName) {
        return this.getExecObservable(screenName).flatMap((response -> {
            return this.exec(call, screenName, true);
        }));
    }

    @Override
    public <T> Observable<T> executeClientSideLogging(Call<T> call) {
        Logger.d("executeClientSideLogging");
        return Observable.just(NetworkUtils.checkInternet(this.mConnectivityManager, this.mRxEventBus)).flatMap((available) -> {
            if (available) {
                Logger.d("executeClientSideLogging - network available");
                return Observable.fromCallable(() -> {
                    try {
                        Response<T> response = call.execute();
                        if (response.isSuccessful()) {
                            Logger.d("executeClientSideLogging - successful");
                            return response.body();
                        } else {
                            throw this.mServiceExceptionHandler.mapApiException(response);
                        }
                    } catch (Exception e) {
                        throw this.mServiceExceptionHandler.mapException(e);
                    }
                });
            } else {
                Logger.d("executeClientSideLogging- network not available");
                return Observable.error(new ApplicationException(Error.ErrorCode.CODE_NO_NETWORK_EXCEPTION));
            }
        }).map((t) -> {
            Logger.d("done executeClientSideLogging");
            return t;
        });
    }

    @Override
    public void setReInitEligibility(Function<String, Observable<?>> encryptionEligibility) {
        this.mReInitEligibility = encryptionEligibility;
    }

    private <T> Observable<T> exec(Call<T> call, String screenName, boolean calculateTime) {
        long startTime = System.currentTimeMillis();

        return Observable.fromCallable(() -> {
            try {
                Response<T> response = call.execute();
                if (response.isSuccessful()) {
                    Logger.d("successful");
                    if (calculateTime) {
                        String url = call.request().url().toString();
                        //Logger.i("url : " + url.toString());
                        if (!url.contains(".json")) {
                            Logger.i("url : not contains JSON");
                            long endTime = System.currentTimeMillis();
                            long diff = endTime - startTime;
                            //TODO : can add log tracker : time and screenName
                        }
                    }
                    //Logger.i("API RESPONSE MESSAGE : " + response.message());
                    //Logger.i("API RESPONSE CODE : " + response.code());
                    return response.body();
                } else {
                    throw this.mServiceExceptionHandler.mapApiException(response);
                }
            } catch (Exception exception) {
                throw this.mServiceExceptionHandler.mapException(exception);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public <T> Observable<?> getExecObservable(String screenName) {
        return Observable.just(NetworkUtils.checkInternet(this.mConnectivityManager, this.mRxEventBus)).flatMap((available) -> {
            if (available) {
                Logger.d("available");
                return mReInitEligibility.apply(screenName);
            } else {
                Logger.e("network Not available >>>>>>>>>>>>>>>>>");
                return Observable.error(new ApplicationException(Error.ErrorCode.CODE_NO_NETWORK_EXCEPTION));
            }
        });
    }

}
