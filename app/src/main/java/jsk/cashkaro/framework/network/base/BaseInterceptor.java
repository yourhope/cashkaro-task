package jsk.cashkaro.framework.network.base;

import android.text.TextUtils;

import java.util.Iterator;
import java.util.List;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEvent;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.store.CookieStore;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseInterceptor implements Interceptor {
    protected CookieStore mCookieStore;
    private AbstractLocale mLocale;
    private static String SID;

    protected BaseInterceptor(CookieStore cookieStore, AbstractLocale locale) {
        mCookieStore = cookieStore;
        mLocale = locale;

        if (SID == null) {
            SID = String.valueOf(System.currentTimeMillis());
        }
    }

    protected void saveCookie(Response response) {
        if (response.headers().get(CookieStore.SET_COOKIE) != null) {
            List<String> cookies = response.headers().values(CookieStore.SET_COOKIE);
            Iterator iterator = cookies.iterator();
            while ((iterator.hasNext())) {
                String cookie = (String) iterator.next();
                if (cookie.contains("JSESSIONID")) {
                    mCookieStore.setCookie(cookie);
                }
            }
        }
    }

    protected Request addGlobalHeaders(Request request) {
        Headers headers = addHeaders(request.headers());
        return request.newBuilder().headers(headers).build();
    }

    private Headers addHeaders(Headers currentHeader) {
        Headers.Builder builder = currentHeader.newBuilder().add("SID", SID);
        if (this.mCookieStore.getCookie() != null) {
            builder.add(CookieStore.COOKIE, mCookieStore.getCookie());
        }

        if (this.mCookieStore.getCookie() != null && !TextUtils.isEmpty(mLocale.getLocale())) {
            builder.add(CookieStore.COOKIE, CookieStore.LOCALE + "=" + this.mLocale.getLocale());
        }

        return builder.build();
    }

    protected String requestBodyToString(RequestBody requestBody) {
        try {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            return buffer.readUtf8();
        } catch (Exception e) {
           throw new RuntimeException("Exception");
        }

    }

    protected void checkForSessionExpiry(Response response){
        try {
            if(response.code() == 302 && response.headers() != null || response.code()  == 404){
                Logger.i("*****************Session timeout.!!***********");
                if(this.getRxEventBus() != null){
                    this.getRxEventBus().publish(new RxEvent("UI_TIMEOUT",100));
                }
            }
        }catch (Exception e){
            Logger.e("Exception while check session expiry.!!",e.getMessage());
        }
    }
    public RxEventBus getRxEventBus(){
        return null;
    }
}
