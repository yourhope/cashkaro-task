package jsk.cashkaro.framework.network.controller;


import java.util.function.Function;

import io.reactivex.Observable;
import retrofit2.Call;

/**
 * Created by Sathish on 5/22/2021
 */
public interface ServiceController {
    <T> Observable<T> executeInit(Call<T> call);

    <T> Observable<T> execute(Call<T> call, String screenName);

    <T> Observable<T> executeClientSideLogging(Call<T> call);

    void setReInitEligibility(Function<String,Observable<?>> function);

}
