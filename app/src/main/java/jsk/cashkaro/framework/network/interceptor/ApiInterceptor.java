package jsk.cashkaro.framework.network.interceptor;

import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.Objects;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.base.BaseInterceptor;
import jsk.cashkaro.framework.network.store.CookieStore;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Sathish on 5/22/2021
 */
public class ApiInterceptor extends BaseInterceptor {

    RxEventBus mRxEventBus;

    public ApiInterceptor(CookieStore cookieStore, RxEventBus rxEventBus, AbstractLocale locale) {
        super(cookieStore, locale);
        this.mRxEventBus = rxEventBus;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = addGlobalHeaders(chain.request());
        //TODO : If encryption required, here can do
        Response response = chain.proceed(request);
        checkForSessionExpiry(response);
        processResponse(response);
        saveCookie(response);
        return response;
    }

    private void processResponse(Response response) throws IOException {
        try {
            JsonParser parser = new JsonParser();
            ResponseBody responseBody = response.peekBody(Long.MAX_VALUE);
            //Logger.i("API RESPONSE\n" + responseBody.string());
        }catch (Exception e){
            Logger.e(Objects.requireNonNull(e.getMessage()));
        }
    }

    public RxEventBus getRxEventBus() {
        return mRxEventBus;
    }
}
