package jsk.cashkaro.framework.network.interceptor;

import java.io.IOException;

import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.base.BaseInterceptor;
import jsk.cashkaro.framework.network.store.CookieStore;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Sathish on 5/22/2021
 */
public class InitInterceptor extends BaseInterceptor {
    RxEventBus mRxEventBus;

    public InitInterceptor(CookieStore cookieStore, RxEventBus rxEventBus, AbstractLocale locale){
        super(cookieStore, locale);
        this.mRxEventBus = rxEventBus;
    }
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = addGlobalHeaders(chain.request());
        Response response =chain.proceed(request);
        checkForSessionExpiry(response);
        saveCookie(response);
        return response;
    }

    public RxEventBus getRxEventBus() {
        return mRxEventBus;
    }
}
