package jsk.cashkaro.framework.network.interceptor;

import java.io.IOException;

import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.base.BaseInterceptor;
import jsk.cashkaro.framework.network.store.CookieStore;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Sathish on 5/22/2021
 */
public class LogInterceptor extends BaseInterceptor {

    public LogInterceptor(CookieStore cookieStore, AbstractLocale locale) {
        super(cookieStore, locale);
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = addGlobalHeaders(chain.request());
        Response response = chain.proceed(request);
        checkForSessionExpiry(response);
        return response;
    }
}
