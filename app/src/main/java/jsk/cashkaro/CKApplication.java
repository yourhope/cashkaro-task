package jsk.cashkaro;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import androidx.fragment.app.Fragment;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;
import jsk.cashkaro.common.di.component.DaggerAppComponent;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.task.BuildConfig;


/**
 * Created by Sathish on 5/22/2021
 */
public class CKApplication extends Application implements HasActivityInjector, HasServiceInjector, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingInjector;

    @Inject
    DispatchingAndroidInjector<Service> serviceDispatchingInjector;
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        Logger.init(BuildConfig.DEBUG, "Logger");

        initRxErrorHandler();
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceDispatchingInjector;
    }

    private void initRxErrorHandler() {
        RxJavaPlugins.setErrorHandler(error -> {
            if (error != null) {
                if (error instanceof UndeliverableException) {
                    Logger.e("Undeliverable Exception received");
                } else if (error instanceof OnErrorNotImplementedException) {
                    //to avoid crash
                    Logger.e("************* AVOIDED FROM CRASH ***************");
                    Logger.e("CRASH: OnErrorNotImplementedException \n" + error.getMessage());
                } else {
                    Logger.e(error.getMessage());
                    Objects.requireNonNull(Thread.currentThread().getUncaughtExceptionHandler()).uncaughtException(Thread.currentThread(), error);
                }
            } else {
                Logger.e("RX Error Handler - null in Application");
            }
        });
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingInjector;
    }
}
