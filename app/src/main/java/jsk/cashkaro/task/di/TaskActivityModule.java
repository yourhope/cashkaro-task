package jsk.cashkaro.task.di;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.common.di.ViewModelProviderFactory;
import jsk.cashkaro.framework.common.di.annotation.ActivityScope;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.task.activity.TaskActivity;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.data.service.AuthService;
import jsk.cashkaro.task.navigation.TaskNavigationController;
import jsk.cashkaro.task.viewmodel.TaskActivityViewModel;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public class TaskActivityModule {

    @Provides
    TaskActivityViewModel taskActivityViewModel(TaskManager taskManager, AuthService authService, RxEventBus rxEventBus, Gson gson) {
        return new TaskActivityViewModel(taskManager, authService, rxEventBus, gson);
    }

    @Provides
    ViewModelProvider.Factory provideTaskActivityViewModelFactory(TaskActivityViewModel taskActivityViewModel) {
        return new ViewModelProviderFactory<>(taskActivityViewModel);
    }

    @Provides
    @ActivityScope
    public TaskNavigationController provideTaskNavigationController(TaskActivity activity, Gson gson) {
        return new TaskNavigationController(activity, gson);
    }
}
