package jsk.cashkaro.task.di;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.common.di.ViewModelProviderFactory;
import jsk.cashkaro.framework.common.di.annotation.ActivityScope;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.task.activity.SharedActivity;
import jsk.cashkaro.task.activity.TaskActivity;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.data.service.AuthService;
import jsk.cashkaro.task.navigation.SharedNavigationController;
import jsk.cashkaro.task.navigation.TaskNavigationController;
import jsk.cashkaro.task.viewmodel.SharedActivityViewModel;
import jsk.cashkaro.task.viewmodel.TaskActivityViewModel;

/**
 * Created by Sathish on 5/25/2021
 */
@Module
public class SharedActivityModule {

    @Provides
    SharedActivityViewModel sharedActivityViewModel(RxEventBus rxEventBus, Gson gson) {
        return new SharedActivityViewModel(rxEventBus, gson);
    }

    @Provides
    ViewModelProvider.Factory provideSharedActivityViewModelFactory(SharedActivityViewModel sharedActivityViewModel) {
        return new ViewModelProviderFactory<>(sharedActivityViewModel);
    }

    @Provides
    @ActivityScope
    public SharedNavigationController provideSharedNavigationController(SharedActivity activity, Gson gson) {
        return new SharedNavigationController(activity, gson);
    }
}
