package jsk.cashkaro.task.di;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import jsk.cashkaro.common.di.ViewModelProviderFactory;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.task.fragment.SplashFragment;
import jsk.cashkaro.task.viewmodel.SplashFragmentViewModel;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public class SplashFragmentModule {

    @Module
    public static abstract class SplashFragmentProvider {
        @ContributesAndroidInjector(modules = SplashFragmentModule.class)
        abstract SplashFragment provideSplashFragment();
    }

    @Provides
    SplashFragmentViewModel splashFragmentViewModel(RxEventBus rxEventBus, Gson gson) {
        return new SplashFragmentViewModel(rxEventBus, gson);
    }

    @Provides
    @Named("FRAGMENT")
    ViewModelProvider.Factory provideSplashFragmentViewModel(SplashFragmentViewModel splashFragmentViewModel) {
        return new ViewModelProviderFactory<>(splashFragmentViewModel);
    }

}
