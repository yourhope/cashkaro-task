package jsk.cashkaro.task.di;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import jsk.cashkaro.common.di.ViewModelProviderFactory;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.fragment.TaskFragment;
import jsk.cashkaro.task.viewmodel.SplashFragmentViewModel;
import jsk.cashkaro.task.viewmodel.TaskFragmentViewModel;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public class TaskFragmentModule {

    @Module
    public static abstract class TaskFragmentProvider {
        @ContributesAndroidInjector(modules = TaskFragmentModule.class)
        abstract TaskFragment provideTaskFragment();
    }

    @Provides
    TaskFragmentViewModel TaskViewModel(TaskManager TaskManager, RxEventBus rxEventBus, Gson gson) {
        return new TaskFragmentViewModel(TaskManager, rxEventBus, gson);
    }

    @Provides
    @Named("FRAGMENT")
    ViewModelProvider.Factory provideTaskFragmentViewModel(TaskFragmentViewModel taskFragmentViewModel) {
        return new ViewModelProviderFactory<>(taskFragmentViewModel);
    }
}
