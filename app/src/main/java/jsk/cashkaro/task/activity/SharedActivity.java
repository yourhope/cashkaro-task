package jsk.cashkaro.task.activity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import jsk.cashkaro.common.base.BaseBindingActivity;
import jsk.cashkaro.common.di.navigation.BaseNavigationController;
import jsk.cashkaro.database.DatabaseClient;
import jsk.cashkaro.database.TaskEntity;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.ui.adapter.CKSharedRecyclerAdapter;
import jsk.cashkaro.framework.ui.adapter.CKShimmerAdapter;
import jsk.cashkaro.framework.ui.listener.SelectionListener;
import jsk.cashkaro.framework.ui.utils.CKUtils;
import jsk.cashkaro.framework.ui.views.CKCustomTextView;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.databinding.ActivitySharedBinding;
import jsk.cashkaro.task.model.SharedDataModel;
import jsk.cashkaro.task.navigation.SharedNavigationController;
import jsk.cashkaro.task.viewmodel.SharedActivityViewModel;

/**
 * Created by Sathish on 5/25/2021
 */
public class SharedActivity extends BaseBindingActivity<SharedActivityViewModel, ActivitySharedBinding> implements HasSupportFragmentInjector {

    @Inject
    ViewModelProvider.Factory mViewModelProviderFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    SharedNavigationController mSharedNavigationController;

    SharedActivityViewModel mViewModel;
    ActivitySharedBinding mBinding;

    private CKSharedRecyclerAdapter ckSharedRecyclerAdapter;
    private CKCustomTextView mDeleteTv;

    public static SharedActivity newInstance() {
        return new SharedActivity();
    }

    @Override
    public SharedActivityViewModel getViewModel() {
        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(SharedActivityViewModel.class);
        return mViewModel;
    }

    @Override
    public BaseNavigationController getNavigationController() {
        return mSharedNavigationController;
    }

    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showShimmer();
        getSharedItems();
        doActions();
    }

    private void doActions() {
        View customToolBar = mBinding.customToolBar;
        mDeleteTv = customToolBar.findViewById(R.id.deleteTv);
        mDeleteTv.setOnClickListener(v -> {
            if (ckSharedRecyclerAdapter != null && ckSharedRecyclerAdapter.getSelectedList().size() > 0) {
                int size = ckSharedRecyclerAdapter.getSelectedList().size();
                ckSharedRecyclerAdapter.deleteSelectedItems();

                mBinding.selectAllTv.setVisibility(View.GONE);
                //TODO : check size mismatch
                if (size >= ckSharedRecyclerAdapter.getItemCount()) {
                    mBinding.noneTv.setVisibility(View.VISIBLE);
                    mBinding.sharedRecycler.setVisibility(View.GONE);
                    mDeleteTv.setVisibility(View.GONE);
                }
                deleteDataFromDb(ckSharedRecyclerAdapter.getSelectedList());
                CKUtils.showToast(getApplicationContext(), size + " items Deleted..");
            } else {
                CKUtils.showToast(getApplicationContext(), "No item Selected");
            }
        });

        mBinding.selectAllTv.setOnClickListener(v -> {
            if (mBinding.selectAllTv.getText().equals("Select All")) {
                ckSharedRecyclerAdapter.selectAll();
                CKUtils.showToast(getApplicationContext(), "All items Selected");
                mBinding.selectAllTv.setText("Clear All");
            } else {    // consider as clear
                ckSharedRecyclerAdapter.clearAll();
                mBinding.selectAllTv.setText("Select All");
                CKUtils.showToast(getApplicationContext(), "All items Cleared");
            }
        });
    }

    @Override
    protected ActivitySharedBinding setupDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_shared);
        mBinding.setViewModel(getViewModel());
        return mBinding;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Logger.d("back pressed");
    }

    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private void showShimmer() {
        CKUtils.setRecyclerLayoutManager(this, mBinding.sharedRecycler);
        CKShimmerAdapter shimmerAdapter = new CKShimmerAdapter(this, 10);
        mBinding.sharedRecycler.setAdapter(shimmerAdapter);
    }

    public void getSharedItems() {
        //Storing the data
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {
            //Background work here
            List<TaskEntity> taskList = DatabaseClient
                    .getInstance(getApplicationContext())
                    .getAppDatabase()
                    .taskDao()
                    .getAll();
            handler.post(() -> {
                //UI Thread work here
                setupAdapter(taskList);
            });
        });
    }

    private void setupAdapter(List<TaskEntity> taskList) {
        if (taskList.size() > 0) {
            mBinding.noneTv.setVisibility(View.GONE);
            mBinding.sharedRecycler.setVisibility(View.VISIBLE);
            List<SharedDataModel> dataModels = new ArrayList<>();

            for (TaskEntity taskEntity : taskList) {
                SharedDataModel sharedDataModel = new SharedDataModel();
                sharedDataModel.setAppName(taskEntity.getAppName());
                sharedDataModel.setId(taskEntity.getId());
                sharedDataModel.setTime(taskEntity.getTitle());
                sharedDataModel.setTime(taskEntity.getTime());
                sharedDataModel.setImgUrl(taskEntity.getImgUrl());
                sharedDataModel.setSelected(false);

                dataModels.add(sharedDataModel);
            }
            ckSharedRecyclerAdapter = new CKSharedRecyclerAdapter(this, dataModels, new SelectionListener() {
                @Override
                public void isSelected() {
                    if (ckSharedRecyclerAdapter != null && ckSharedRecyclerAdapter.getSelectedList().size() > 0) {
                        mBinding.selectAllTv.setVisibility(View.VISIBLE);
                        mDeleteTv.setVisibility(View.VISIBLE);
                        if (ckSharedRecyclerAdapter.getSelectedList().size() == ckSharedRecyclerAdapter.getItemCount()) {
                            mBinding.selectAllTv.setText("Clear All");
                        }
                    } else {
                        mBinding.selectAllTv.setVisibility(View.GONE);
                        mDeleteTv.setVisibility(View.GONE);
                    }

                }
            });
            mBinding.sharedRecycler.setAdapter(ckSharedRecyclerAdapter);
        } else {
            mBinding.noneTv.setVisibility(View.VISIBLE);
            mBinding.sharedRecycler.setVisibility(View.GONE);
        }
    }

    private void deleteDataFromDb(List<SharedDataModel> dataModelList) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        for(SharedDataModel model:dataModelList){
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setTime(model.getTime());
            taskEntity.setTitle(model.getTitle());
            taskEntity.setImgUrl(model.getImgUrl());
            taskEntity.setAppName(model.getAppName());
            taskEntity.setId(model.getId());
            executor.execute(() -> {
                //Background work here
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .delete(taskEntity);
                //UI Thread work here
                handler.post(this::getSharedItems);
            });
        }
    }
}
