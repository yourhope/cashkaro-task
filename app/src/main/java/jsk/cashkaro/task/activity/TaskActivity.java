package jsk.cashkaro.task.activity;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import jsk.cashkaro.common.Constants;
import jsk.cashkaro.common.base.BaseBindingActivity;
import jsk.cashkaro.common.di.navigation.BaseNavigationController;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.ui.listener.ApplicationSelectorReceiver;
import jsk.cashkaro.framework.ui.utils.CKUtils;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.databinding.ActivityTaskBinding;
import jsk.cashkaro.task.navigation.TaskNavigationController;
import jsk.cashkaro.task.viewmodel.TaskActivityViewModel;

/**
 * Created by Sathish on 5/22/2021
 */
public class TaskActivity extends BaseBindingActivity<TaskActivityViewModel, ActivityTaskBinding> implements HasSupportFragmentInjector {

    @Inject
    ViewModelProvider.Factory mViewModelProviderFactory;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    TaskNavigationController mTaskNavigationController;

    @Inject
    RxEventBus rxEventBus;

    @Inject
    Gson mGson;

    TaskActivityViewModel mViewModel;
    ActivityTaskBinding mBinding;

    public static TaskActivity newInstance(String msg) {
        TaskActivity loginActivity = new TaskActivity();
        Bundle bundle = new Bundle();
        bundle.putString("msg", "TaskAct");
        //TODO receive data
        return loginActivity;
    }

    @Override
    public TaskActivityViewModel getViewModel() {
        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(TaskActivityViewModel.class);
        return mViewModel;
    }

    @Override
    public BaseNavigationController getNavigationController() {
        return mTaskNavigationController;
    }

    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTaskNavigationController.displaySplashScreen();
    }

    @Override
    protected ActivityTaskBinding setupDataBinding() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_task);
        mBinding.setViewModel(getViewModel());
        return mBinding;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Logger.d("back pressed");
    }

    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void shareContent(String aTitleStr, String imgUrl) {
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        //adding data
        String composeMsg = "Title : " + aTitleStr + "\nImage : " + imgUrl + "\nTime : " + CKUtils.getDateAndTime();
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CashKaro");
        shareIntent.putExtra(Intent.EXTRA_TEXT, composeMsg);

        Intent receiverIntent = new Intent(this, ApplicationSelectorReceiver.class);
        receiverIntent.putExtra("title", aTitleStr);
        receiverIntent.putExtra("imgUrl", imgUrl);
        receiverIntent.putExtra("time", CKUtils.getDateAndTime());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, receiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent chooser = Intent.createChooser(shareIntent, null, pendingIntent.getIntentSender());
        startActivity(chooser);
    }
}

