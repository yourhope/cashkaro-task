package jsk.cashkaro.task.data.service;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Sathish on 5/22/2021
 */
public interface AuthService {
    @GET
    Call<Void> getContents(String url);
}
