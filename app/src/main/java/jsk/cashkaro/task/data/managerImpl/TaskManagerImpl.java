package jsk.cashkaro.task.data.managerImpl;


import com.google.gson.Gson;

import io.reactivex.Observable;
import jsk.cashkaro.common.manager.BaseManagerImpl;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.network.controller.ServiceController;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.data.service.TaskService;
import okhttp3.ResponseBody;

/**
 * Created by Sathish on 5/22/2021
 */
public class TaskManagerImpl extends BaseManagerImpl implements TaskManager {

    private TaskService mTaskService;
    private Gson mGson;
    private ServiceController mServiceController;

    public TaskManagerImpl(ServiceController serviceController, TaskService taskService, Gson gson) {
        mServiceController = serviceController;
        mTaskService = taskService;
        mGson = gson;
    }

    @Override
    public Observable<ResponseBody> getOpenAPIList() {
        return mServiceController.execute(mTaskService.getOpenAPIList(), "Task")
                .map(contentObject -> {
                    //Logger.i("Sathish - API data\n" + contentObject);
                    return contentObject;
                });
    }
}
