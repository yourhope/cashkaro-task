package jsk.cashkaro.task.data.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Sathish on 5/22/2021
 */
public interface TaskService  {
    @GET("v2/list.json")
    Call<ResponseBody> getOpenAPIList();
}
