package jsk.cashkaro.task.data.manager;


import io.reactivex.Observable;
import jsk.cashkaro.framework.common.manager.BaseManager;
import okhttp3.ResponseBody;

/**
 * Created by Sathish on 5/22/2021
 */
public interface TaskManager extends BaseManager {
    //Network Calls
    Observable<ResponseBody> getOpenAPIList();
}
