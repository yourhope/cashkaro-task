package jsk.cashkaro.task.viewmodel;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import jsk.cashkaro.common.Constants;
import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.error.ApplicationException;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.common.utils.rx.RxSchedulerProvider;
import jsk.cashkaro.framework.common.utils.rx.SingleLiveEvent;
import jsk.cashkaro.framework.ui.model.TaskDataItem;
import jsk.cashkaro.task.data.manager.TaskManager;


/**
 * Created by Sathish on 5/22/2021
 */
public class TaskFragmentViewModel extends BaseViewModel {

    private RxEventBus mRxEventBus;
    private Gson mGson;
    TaskManager mTaskManager;

    private SingleLiveEvent<List<TaskDataItem>> mTaskDataItemLiveData = new SingleLiveEvent<>();

    public TaskFragmentViewModel(TaskManager taskManager, RxEventBus rxEventBus, Gson gson) {
        mTaskManager = taskManager;
        mRxEventBus = rxEventBus;
        mGson = gson;
    }

    public void getOpenAPIList() {
        try {
            if (getCompositeDisposable() != null) {
                Disposable disposable = mTaskManager.getOpenAPIList()
                        .doOnSubscribe(s -> setIsLoading(true))
                        .doOnTerminate(() -> setIsLoading(false))
                        .subscribeOn(RxSchedulerProvider.io())
                        .observeOn(RxSchedulerProvider.ui())
                        .subscribe(responseBody -> {
                            String lContent = responseBody.string();
                            if (!TextUtils.isEmpty(lContent)) {
                                mTaskDataItemLiveData.setValue(getModelData(lContent));
                            } else {
                                mTaskDataItemLiveData.setValue(new ArrayList<>());
                            }
                        }, error -> {
                            mTaskDataItemLiveData.setValue(new ArrayList<>());
                        });
                getCompositeDisposable().add(disposable);
            }
        } catch (OnErrorNotImplementedException | ApplicationException e){
            mTaskDataItemLiveData.setValue(new ArrayList<>());
        }
    }


    public List<TaskDataItem> getModelData(String lContent) {
        List<TaskDataItem> taskDataItems = new ArrayList<>();
        try {
            //taking first 30 objects :  for example
            JSONObject rootObj = new JSONObject(lContent);
            Iterator keysToCopyIterator = rootObj.keys();
            List<String> keysList = new ArrayList<>();
            while (keysToCopyIterator.hasNext()) {
                String key = (String) keysToCopyIterator.next();
                //Logger.i("key : " + keysList.size() + " : " + key);
                keysList.add(key);
                JSONObject innerObj = rootObj.getJSONObject(key);
                JSONObject versionObj = innerObj.getJSONObject("versions");
                Iterator innerIterator = versionObj.keys();
                while (innerIterator.hasNext()) {
                    String vKey = (String) innerIterator.next();
                    //Logger.i("vkey : " + keysList.size() + " : " + vKey);
                    JSONObject vNameObj = versionObj.getJSONObject(vKey);
                    JSONObject infoObj = vNameObj.getJSONObject("info");
                    TaskDataItem taskDataItem = new TaskDataItem();
                    String desc = infoObj.optString("description");
                    String title = infoObj.optString("title");
                    String added = vNameObj.optString("added");
                    String updated = vNameObj.optString("updated");
                    JSONObject urlObj = infoObj.optJSONObject("x-logo");
                    if (urlObj != null) {
                        String url = urlObj.optString("url");
                        Logger.i("URL : " + url);
                        taskDataItem.setImgUrl(url);
                    }
                    taskDataItem.setAge(added);
                    taskDataItem.setLastLogin(updated);
                    taskDataItem.setMessage(desc);
                    taskDataItem.setName(title);
                    taskDataItems.add(taskDataItem);
                }

                if (keysList.size() == 20)  // for minimum load
                    break;
            }
        } catch (JSONException e) {
            Logger.i("key : exception");
        }
        return taskDataItems;
    }

    public SingleLiveEvent<List<TaskDataItem>> getTaskDataItemLiveData() {
        return mTaskDataItemLiveData;
    }
}
