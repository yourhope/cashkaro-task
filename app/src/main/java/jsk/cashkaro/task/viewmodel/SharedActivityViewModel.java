package jsk.cashkaro.task.viewmodel;

import com.google.gson.Gson;

import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;

/**
 * Created by Sathish on 5/22/2021
 */
public class SharedActivityViewModel extends BaseViewModel {

    private RxEventBus mRxEventBus;
    private Gson mGson;

    public SharedActivityViewModel(RxEventBus rxEventBus, Gson gson) {
        mRxEventBus = rxEventBus;
        mGson = gson;
    }
}
