package jsk.cashkaro.task.viewmodel;

import android.text.TextUtils;

import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.common.utils.rx.RxSchedulerProvider;
import jsk.cashkaro.framework.common.utils.rx.SingleLiveEvent;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.data.service.AuthService;

/**
 * Created by Sathish on 5/22/2021
 */
public class TaskActivityViewModel extends BaseViewModel {

    private RxEventBus mRxEventBus;
    private Gson mGson;
    private TaskManager mTaskManager;
    private AuthService mAuthService;

    public TaskActivityViewModel(TaskManager loginManager, AuthService authService, RxEventBus rxEventBus, Gson gson) {
        mTaskManager = loginManager;
        mAuthService = authService;
        mRxEventBus = rxEventBus;
        mGson = gson;
    }
}
