package jsk.cashkaro.task.viewmodel;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.disposables.Disposable;
import jsk.cashkaro.common.Constants;
import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.common.utils.rx.RxSchedulerProvider;
import jsk.cashkaro.framework.common.utils.rx.SingleLiveEvent;
import jsk.cashkaro.framework.ui.model.TaskDataItem;
import jsk.cashkaro.framework.ui.model.TaskItemModel;
import jsk.cashkaro.task.data.manager.TaskManager;

/**
 * Created by Sathish on 5/22/2021
 */
public class SplashFragmentViewModel extends BaseViewModel {

    private RxEventBus mRxEventBus;
    private Gson mGson;

    public SplashFragmentViewModel(RxEventBus rxEventBus, Gson gson) {
        mRxEventBus = rxEventBus;
        mGson = gson;
    }
}
