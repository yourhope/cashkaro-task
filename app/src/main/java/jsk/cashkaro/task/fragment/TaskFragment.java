package jsk.cashkaro.task.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import jsk.cashkaro.common.base.BaseBindingFragment;
import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.ui.adapter.CKRecyclerAdapter;
import jsk.cashkaro.framework.ui.adapter.CKShimmerAdapter;
import jsk.cashkaro.framework.ui.model.TaskDataItem;
import jsk.cashkaro.framework.ui.utils.CKUtils;
import jsk.cashkaro.framework.worker.CKWorker;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.activity.SharedActivity;
import jsk.cashkaro.task.activity.TaskActivity;
import jsk.cashkaro.task.databinding.TaskFragmentBinding;
import jsk.cashkaro.task.viewmodel.TaskFragmentViewModel;


/**
 * Created by Sathish on 5/22/2021
 */
public class TaskFragment extends BaseBindingFragment<TaskFragmentViewModel, TaskFragmentBinding> {

    private TaskFragmentViewModel mViewModel;
    private TaskFragmentBinding mBinding;

    @Named("FRAGMENT")
    @Inject
    ViewModelProvider.Factory mViewModelProvider;

    @Inject
    Gson mGson;

    @Inject
    RxEventBus rxEventBus;

    public static TaskFragment newInstance() {
        return new TaskFragment();
    }

    @Override
    protected TaskFragmentBinding setUpDataBinding() {
        return mBinding;
    }

    @Override
    protected BaseViewModel getViewModel() {
        mViewModel = mViewModelProvider.create(TaskFragmentViewModel.class);
        return mViewModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = TaskFragmentBinding.inflate(inflater, container, false);
        initViews();
        initWorkerManager();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBinding.setViewModel(mViewModel);
    }

    private void initViews() {
        showShimmer();
        mViewModel.getOpenAPIList();
        mViewModel.getTaskDataItemLiveData().observe(this, list -> {
            Logger.i("sathish - total item : " + list.size());
            if (list.size() > 0) {
                mBinding.errorLL.setVisibility(View.GONE);
                mBinding.taskRecycler.setVisibility(View.VISIBLE);
                CKRecyclerAdapter adapter = new CKRecyclerAdapter(getContext(), makeOriginalData(list), (viewHolder, view, position, dataItem) -> {
                    if (dataItem != null) {
                        String lTitle = dataItem.getName();
                        String lImgUrl = dataItem.getImgUrl();
                        if (getActivity() != null) {
                            ((TaskActivity) getActivity()).shareContent(lTitle, lImgUrl);
                        }
                    }
                });
                mBinding.taskRecycler.setAdapter(adapter);
            } else {
                mBinding.taskRecycler.setVisibility(View.GONE);
                mBinding.errorLL.setVisibility(View.VISIBLE);
                mBinding.retryBtn.setOnClickListener(v -> initViews());
            }
        });

        //Temp nav
        mBinding.actionBar.nxtBtn.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SharedActivity.class);
            startActivity(intent);
            if (getActivity() != null) {
                getActivity().overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out);
            }
        });

    }

    private List<TaskDataItem> getDummyItems() {
        List<TaskDataItem> recyclerItems = new ArrayList<>();

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_SINGLE_ITEM, "message"));
        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_INBOX_ITEM, "", "msg 2", "date 2", "name 2"));

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_LEFT_ICON, "", "message left"));
        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_LEFT_ICON, "", "message left2"));
        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_ITEM, "", "name 1", "age 1", "caste 1", "lastlogin 1"));

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_RIGHT_ICON, "", "message right"));

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_ITEM, "", "name 2", "age 2", "caste 2", "lastlogin 2"));

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_INBOX_ITEM, "", "msg 1", "date 1", "name 1"));

        recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_RIGHT_ICON, "", "message right2"));

        return recyclerItems;
    }

    private List<TaskDataItem> makeOriginalData(List<TaskDataItem> list) {
        List<TaskDataItem> recyclerItems = new ArrayList<>();
        if (list.size() > 0) {
            for (int item = 0; item < list.size(); item++) {

                TaskDataItem taskDataItem = list.get(item);
                String desc = taskDataItem.getMessage();
                String title = taskDataItem.getName();
                String added = taskDataItem.getAge();
                String updated = taskDataItem.getLastLogin();
                String url = taskDataItem.getImgUrl();
                //adding dummy data - might or Sponsored/adv
                if (item == 0) {
                    recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_SINGLE_ITEM, title));
                } else if (item % 5 == 0) {
                    recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_RIGHT_ICON, url, title));
                } else if (item % 7 == 0) {
                    recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_TEXT_WITH_LEFT_ICON, url, title));
                }

                //adding the oriinal data
                if (item % 3 == 0) {
                    recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_ITEM, url, title, added, desc, updated));
                } else {
                    recyclerItems.add(new TaskDataItem(CKRecyclerAdapter.JS_PROFILE_INBOX_ITEM, url, desc, added, title));
                }
            }
        }
        return recyclerItems;
    }

    private void showShimmer() {
        CKUtils.setRecyclerLayoutManager(getContext(), mBinding.taskRecycler);
        CKShimmerAdapter shimmerAdapter = new CKShimmerAdapter(getContext(), 10);
        mBinding.taskRecycler.setAdapter(shimmerAdapter);
    }

    private void initWorkerManager() {
        //Testing manual trigger
        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(CKWorker.class).build();
        mBinding.actionBar.notifIcon.setOnClickListener(v -> {
            //Enqueuing the work request
            WorkManager.getInstance().enqueue(workRequest);
        });

        /*
        //scheduling the work
        Data d = new Data.Builder().putInt("100", 150).build();

        //creating 4 AM calender
        Calendar c10 = Calendar.getInstance();
        c10.set(Calendar.AM_PM, Calendar.AM);
        c10.set(Calendar.HOUR, 10);
        c10.set(Calendar.MINUTE, 0);

        long currentTime = System.currentTimeMillis();
        long specificTimeToTrigger10 = c10.getTimeInMillis();
        long delayToPass10 = specificTimeToTrigger10 - currentTime;
        Logger.i("10 AM Request " + delayToPass10);
        OneTimeWorkRequest workRequest1 =
                new OneTimeWorkRequest.Builder(CKWorker.class)
                        .setInputData(d)
                        .addTag("worker1")
                        .setInitialDelay(delayToPass10, TimeUnit.MILLISECONDS)
                        .build();


        //creating 4 PM calender
        Calendar c4 = Calendar.getInstance();
        c10.set(Calendar.AM_PM, Calendar.PM);
        c10.set(Calendar.HOUR, 4);
        c10.set(Calendar.MINUTE, 0);

        long specificTimeToTrigger4 = c4.getTimeInMillis();
        long delayToPass4 = specificTimeToTrigger4 - currentTime;
        Logger.i("4 PM Request " + delayToPass4);

        OneTimeWorkRequest workRequest2 =
                new OneTimeWorkRequest.Builder(CKWorker.class)
                        .setInputData(d)
                        .setInitialDelay(delayToPass4, TimeUnit.MILLISECONDS)
                        .build();

        WorkManager.getInstance().
                beginWith(workRequest1)
                .then(workRequest2)
                .enqueue();
        */

        //Periodic Worker
        int hourOfTheDay10 = 10; // When to run 10PM job
        int hourOfTheDay4 = 4; // When you run 4PM job
        int repeatInterval = 1; // In days

        String tag10AM = "TAG_10_AM";
        String tag4PM = "TAG_4_PM";
        long flexTime10 = calculateDuration(hourOfTheDay10, repeatInterval, Calendar.AM);
        long flexTime4 = calculateDuration(hourOfTheDay4, repeatInterval, Calendar.PM);
        // 10 am request
        PeriodicWorkRequest periodic10IntervalRequest = new PeriodicWorkRequest.Builder(CKWorker.class,
                repeatInterval, TimeUnit.DAYS,
                flexTime10, TimeUnit.MILLISECONDS)
                .addTag(tag10AM)
                .build();

        WorkManager.getInstance().enqueueUniquePeriodicWork(tag10AM, ExistingPeriodicWorkPolicy.REPLACE, periodic10IntervalRequest);

        // 4pm request
        PeriodicWorkRequest periodic4IntervalRequest = new PeriodicWorkRequest.Builder(CKWorker.class,
                repeatInterval, TimeUnit.DAYS,
                flexTime4, TimeUnit.MILLISECONDS)
                .addTag(tag4PM)
                .build();

        WorkManager.getInstance().enqueueUniquePeriodicWork(tag4PM, ExistingPeriodicWorkPolicy.REPLACE, periodic4IntervalRequest);
    }

    private long calculateDuration(int hourOfTheDay, int periodInDays, int timeAmPm) {
        // Initialize the calendar with today and the preferred time to run the job.
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.AM_PM, timeAmPm);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfTheDay);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        // Initialize a calendar with now.
        Calendar cal2 = Calendar.getInstance();
        if (cal2.getTimeInMillis() < calendar.getTimeInMillis()) {
            // Add the worker periodicity.
            cal2.setTimeInMillis(cal2.getTimeInMillis() + TimeUnit.DAYS.toMillis(periodInDays));
        }

        long delta = (cal2.getTimeInMillis() - calendar.getTimeInMillis());
        return (Math.max(delta, PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS));
    }
}
