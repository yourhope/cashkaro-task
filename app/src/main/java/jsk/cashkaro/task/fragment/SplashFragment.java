package jsk.cashkaro.task.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Named;

import jsk.cashkaro.common.Constants;
import jsk.cashkaro.common.base.BaseBindingFragment;
import jsk.cashkaro.common.base.BaseViewModel;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.task.databinding.SplashFragmentBinding;
import jsk.cashkaro.task.navigation.TaskNavigationController;
import jsk.cashkaro.task.viewmodel.SplashFragmentViewModel;

import static jsk.cashkaro.common.Constants.Value.SIGNUP_ANIM_DELAY;

/**
 * Created by Sathish on 5/22/2021
 */
public class SplashFragment extends BaseBindingFragment<SplashFragmentViewModel, SplashFragmentBinding> {

    private SplashFragmentViewModel mViewModel;
    private SplashFragmentBinding mBinding;

    @Named("FRAGMENT")
    @Inject
    ViewModelProvider.Factory mViewModelProvider;

    @Inject
    Gson mGson;

    @Inject
    RxEventBus rxEventBus;

    @Inject
    TaskNavigationController mTaskNavigationController;

    private Context mContext;

    public static SplashFragment newInstance() {
        SplashFragment splashFragment = new SplashFragment();
        return splashFragment;
    }

    @Override
    protected SplashFragmentBinding setUpDataBinding() {
        return mBinding;
    }

    @Override
    protected BaseViewModel getViewModel() {
        mViewModel = mViewModelProvider.create(SplashFragmentViewModel.class);
        return mViewModel;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SplashFragmentBinding.inflate(inflater, container, false);
        mBinding.setViewmodel(mViewModel);
        return mBinding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadLogoAnim();
        navigateToSignUpWithDelay();
    }

    private void navigateToSignUpWithDelay() {
        new Handler(Looper.getMainLooper()).postDelayed(() -> mTaskNavigationController.addTaskFragment(null), Constants.Value.SIGNUP_ANIM_DELAY);
    }

    private void loadLogoAnim() {
        Animation animSlide = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
        mBinding.splashImg.startAnimation(animSlide);
    }
}
