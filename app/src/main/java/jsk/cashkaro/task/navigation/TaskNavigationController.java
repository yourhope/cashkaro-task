package jsk.cashkaro.task.navigation;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;

import jsk.cashkaro.common.di.navigation.BaseNavigationController;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.task.R;
import jsk.cashkaro.task.activity.TaskActivity;
import jsk.cashkaro.task.fragment.SplashFragment;
import jsk.cashkaro.task.fragment.TaskFragment;


/**
 * Created by Sathish on 5/22/2021
 */
public class TaskNavigationController extends BaseNavigationController {

    private Context mContext;
    private FragmentManager mFragmentManager;
    private Gson mGson;
    private TaskFragment mTaskFragment;

    private static final String TRANSACTION_BACKSTACK_INIT = "init";
    private static final String TRANSACTION_BACKSTACK_SPLASH = "Splash";

    public TaskNavigationController(TaskActivity activity, Gson gson) {
        super(activity);
        this.mGson = gson;
        this.mFragmentManager = activity.getSupportFragmentManager();
        this.mContext = activity;
    }

    @Override
    public int getContainerId() {
        return R.id.container_fl;
    }

    public TaskFragment getTaskFragment() {
        return mTaskFragment;
    }

    public void setTaskFragment(TaskFragment taskFragment) {
        this.mTaskFragment = taskFragment;
    }

    public void popBackStack() {
        try {
            mFragmentManager.popBackStack();
        } catch (Exception e) {
            Logger.e("error while popBackStack", e.getMessage());
        }
    }

    public void popBackStackImmediate() {
        try {
            mFragmentManager.popBackStackImmediate();
        } catch (Exception e) {
            Logger.e("Error while popBackStackImmediate", e.getMessage());
        }
    }

    public void popBackStack(String tag) {
        try {
            mFragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {
            Logger.i("Error while popBackStack", e.getMessage());
        }
    }

    public void addTaskFragment(Bundle bundle) {
        TaskFragment taskFragment = TaskFragment.newInstance();
        addFragment(bundle, taskFragment, TaskFragment.class);
    }

    private void addFragment(Bundle bundle, Fragment fragment, Class fragmentClass) {
        try {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            Logger.i("addFragment " + fragmentClass.getSimpleName());
            boolean isFragmentAlreadyAdded = removeFragmentIfExist(fragmentClass);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            switch (fragmentClass.getSimpleName()) {
                //can do any type animation for each fragment based on case
                case "TaskFragment":
                    isFragmentAlreadyAdded = true;
                default:
                    break;
            }
            fragmentTransaction.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out);
            fragmentTransaction.add(R.id.container_fl, fragment, fragmentClass.getSimpleName());
            fragmentTransaction.addToBackStack(fragmentClass.getSimpleName());
            fragmentTransaction.commit();
            if (isFragmentAlreadyAdded) {
                // mFragmentManager.executePendingTransactions();
            }
        } catch (IllegalStateException e){

        }
    }

    private boolean removeFragmentIfExist(Class fragmentClass) {
        for (Fragment existFragment : mFragmentManager.getFragments()) {
            if (existFragment != null && TextUtils.equals(existFragment.getClass().getSimpleName(), fragmentClass.getSimpleName())) {
                mFragmentManager.beginTransaction().remove(existFragment).commit();
                return true;
            } else {
                Logger.d("fragment unavailable");
            }
        }
        return false;
    }

    public void displaySplashScreen() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(getContainerId(), SplashFragment.newInstance(), SplashFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(TRANSACTION_BACKSTACK_SPLASH);
        fragmentTransaction.commit();
    }
}
