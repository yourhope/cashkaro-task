package jsk.cashkaro.task.navigation;

import android.content.Context;

import androidx.fragment.app.FragmentManager;

import com.google.gson.Gson;

import jsk.cashkaro.common.base.BaseActivity;
import jsk.cashkaro.common.di.navigation.BaseNavigationController;
import jsk.cashkaro.task.activity.SharedActivity;
import jsk.cashkaro.task.fragment.TaskFragment;

/**
 * Created by Sathish on 5/25/2021
 */
/* Dummy Class */
public class SharedNavigationController extends BaseNavigationController {

    private Context mContext;
    private FragmentManager mFragmentManager;
    private Gson mGson;
    private TaskFragment mTaskFragment;

    public SharedNavigationController(SharedActivity activity, Gson gson) {
        super(activity);
        this.mGson = gson;
        this.mFragmentManager = activity.getSupportFragmentManager();
        this.mContext = activity;
    }

    @Override
    public int getContainerId() {
        return 0; //TODO
    }
}
