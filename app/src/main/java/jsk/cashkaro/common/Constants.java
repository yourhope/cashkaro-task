package jsk.cashkaro.common;

/**
 * Created by Sathish on 5/22/2021
 */
public class Constants {

    public static final String APP_DATABASE = "CashKaroTaskDB";

    public static class LogConstants {
        public static final String APP_LOG = "CK LOG";
    }

    public static class FileName {
        public static final String defaultFontName = "fonts/trebuchet-ms.ttf";
        public static final String ItalicFont = "fonts/Trebuchet-Ms-Italic.ttf";
    }

    public static class Value {
        public static final int SIGNUP_ANIM_DELAY = 2000;
        public static final int DOUBLE_TAP_DELAY = 1000;
        public static final int REQUEST_CODE_FOR_SHARE_REQUEST = 500;
        public static final int REQUEST_CODE_FOR_SHARE_RESULT = 501;

        public static final boolean FALSE = false;
        public static final boolean TRUE = true;

        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
    }
}
