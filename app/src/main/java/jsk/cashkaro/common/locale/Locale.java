package jsk.cashkaro.common.locale;

import android.content.Context;
import android.content.SharedPreferences;

import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.locale.AbstractLocale;


/**
 * Created by Sathish on 5/22/2021
 */
public class Locale implements AbstractLocale {

    private static Locale ourInstance;
    private Context mContext;
    private SharedPreferences localStore;
    private String LOCALE = "locale";
    private static String DEFAULT_LOCALE = "en";

    public static Locale getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new Locale(context);
        return ourInstance;
    }

    private Locale(Context context) {
        mContext = context;
        this.localStore = mContext.getSharedPreferences(LOCALE, Context.MODE_PRIVATE);
    }

    public String getLocale() {
        return this.localStore.getString(LOCALE, DEFAULT_LOCALE);
    }

    public void setLocale(String locale) {
        try {
            this.LOCALE = locale;
            this.localStore.edit().putString(LOCALE, locale).apply();
        } catch (Exception e) {
            Logger.e("setup failed");
        }
    }
}
