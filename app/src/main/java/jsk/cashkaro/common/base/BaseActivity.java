package jsk.cashkaro.common.base;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import jsk.cashkaro.common.Constants;
import jsk.cashkaro.common.di.navigation.BaseNavigationController;
import jsk.cashkaro.framework.common.utils.logger.Logger;
import jsk.cashkaro.framework.common.utils.rx.RxEvent;
import jsk.cashkaro.framework.common.utils.rx.RxEventBus;
import jsk.cashkaro.framework.common.utils.rx.RxSchedulerProvider;
import jsk.cashkaro.framework.ui.views.CKCustomTextView;
import jsk.cashkaro.task.R;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseActivity<V extends BaseViewModel> extends AppCompatActivity implements BaseFragment.FragmentCallBack {
    protected V mViewModel;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    protected RxEventBus mEventBus;

    protected static boolean mPaused;
    protected static boolean mStopped;
    protected static boolean mVisible;
    protected static boolean mISOutOfMemory;

    private boolean mDoubleBackToExitPressedOnce = false;
    private Toast mToast = null;

    @SuppressLint("ShowToast")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            AndroidInjection.inject(this);
            super.onCreate(savedInstanceState);

            mViewModel = mViewModel != null ? mViewModel : getViewModel();

            initSubscriptions();

            mToast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        } catch (Exception e) {
            Logger.e("Exception in onCreate : " + e.getMessage());
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(
                newBase.getResources().getConfiguration()
        );
        override.fontScale = 0.95f;
        //applyOverrideConfiguration(override);
    }

    @Override
    protected void onStart() {
        try {
            super.onStart();
            mStopped = false;
            //TODO :session stuff

            subscribeToNetWorkError();
        } catch (Exception e) {
            Logger.e("Exception in onStart : " + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
            mVisible = true;
            if (mPaused) {
                mPaused = false;
                return;
            }
        } catch (Exception e) {
            Logger.e("Exception in onResume : " + e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        try {
            mVisible = false;
            mPaused = true;
            super.onPause();
        } catch (Exception e) {
            Logger.e("Exception in onPause : " + e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        try {
            mStopped = true;
            mPaused = false;
            super.onStop();
        } catch (Exception e) {
            Logger.e("Exception in onStop : " + e.getMessage());
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            Logger.e("Exception in dispatchEvent : " + e.getMessage());
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        FragmentManager fm = getSupportFragmentManager();
        int lCount = 0;
        for (int entry = 0; entry < fm.getBackStackEntryCount(); entry++) {
            Logger.i("Found fragment: " + fm.getClass().getSimpleName() + " : " + fm.getBackStackEntryAt(entry).getId());
            lCount = entry;
        }

        if (lCount == 1) {
            if (mDoubleBackToExitPressedOnce) {
                Logger.d("Closing Activity");
                finish();
            }
            doubleTapExit();
        } else {
            super.onBackPressed();
            Logger.d("back pressed");
        }
        Logger.d("back pressed");
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Logger.d("Closing Activity");
            finish();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }*/

    @Override
    public void onDetach(String var1) {

    }

    @Override
    public void onAttach() {

    }

    public abstract V getViewModel();

    //TODO : add ISeesionManager

    public abstract BaseNavigationController getNavigationController();

    public void subscribeToNetWorkError() {
        Disposable disposable = mEventBus.listenTo(RxEvent.class)
                .subscribeOn(RxSchedulerProvider.computation())
                .observeOn(RxSchedulerProvider.ui())
                .subscribe(rxEvent -> {
                    //TODO : SessionManager
                });
        getViewModel().getCompositeDisposable().add(disposable);
    }

    private void initSubscriptions() {
        compositeDisposable.add(
                mEventBus.listenTo(RxEvent.class)
                        .subscribeOn(RxSchedulerProvider.computation())
                        .observeOn(RxSchedulerProvider.ui())
                        .subscribe(rxEvent -> {
                            try {
                                switch (rxEvent.getCode()) {
                                    case 0://"OUT_OF_MEMORY":
                                        if (!mISOutOfMemory) {
                                            mISOutOfMemory = true;
                                            doReStart();
                                        }
                                        break;
                                    case 1:
                                        //TODO : session timeout
                                        break;
                                    case 2:
                                        //Clear prev API call
                                        getViewModel().clearCompositeDisposable();
                                        break;
                                }
                            } catch (Exception e) {
                                Logger.e( "Error while initSubscriptions" , e.getMessage());
                            }
                        }));

    }

    private void doReStart() {
        try{
            PackageManager packageManager = getPackageManager();
            if(packageManager != null){
                Intent startActivity = packageManager.getLaunchIntentForPackage(getPackageName());
                int OOM_TIME_DELAY = 100;
                int OOM_STATUS = 100;
                if(startActivity != null){
                    startActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    int mPendingIntentId = 223344;
                    PendingIntent pendingIntent = PendingIntent.getActivity(this,mPendingIntentId,startActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC, System.currentTimeMillis()+OOM_TIME_DELAY,pendingIntent);
                    //kill the App
                    System.exit(OOM_STATUS);
                }else {
                    mISOutOfMemory = false;
                }
            }else {
                mISOutOfMemory = false;
            }
        }catch (Exception | Error e){
            mISOutOfMemory = false;
        }
    }

    /**
     * To exit while clicking double time
     */
    private void doubleTapExit() {
        mDoubleBackToExitPressedOnce = true;
        String msg = getResources().getString(R.string.exit_txt);
        showToast(msg);
        new Handler(Looper.getMainLooper()).postDelayed(() -> mDoubleBackToExitPressedOnce = false, Constants.Value.DOUBLE_TAP_DELAY);
    }

    /**
     * To show the Toast
     * @param lMsg : msg to show in toast
     */
    public void showToast(String lMsg) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,  findViewById(R.id.custom_toast_container));
        CKCustomTextView tv = layout.findViewById(R.id.msgTv);
        tv.setText(lMsg);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(layout);
        mToast.show();
    }
}
