package jsk.cashkaro.common.base;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import dagger.android.support.AndroidSupportInjection;
import jsk.cashkaro.framework.common.utils.logger.Logger;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseFragment<V extends BaseViewModel> extends Fragment {
    private BaseActivity mActivity;
    private V mViewModel;

    public BaseFragment() {
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        //Logger.i("onCreate of", ""+this.getClass().getSimpleName());
        this.mViewModel = this.getViewModel();
        this.setHasOptionsMenu(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().setFocusable(true);
        getView().setClickable(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        //Logger.i("onStart of", ""+this.getClass().getSimpleName());
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.i("onStop of", ""+this.getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();
        //Logger.i("onPause of", ""+this.getClass().getSimpleName());
    }

    @Override
    public void onResume() {
        super.onResume();
        //Logger.i("onResume of", ""+this.getClass().getSimpleName());
    }


    public void onAttach(Context context) {
        super.onAttach(context);
        //Logger.i("onAttach of", ""+this.getClass().getSimpleName());
        if (context instanceof BaseActivity) {
            this.mActivity = (BaseActivity) context;
            this.mActivity.onAttach();
        }
    }

    public void onDetach() {
        Logger.i("onDetach of", ""+this.getClass().getSimpleName());
        this.mActivity = null;
        super.onDetach();
    }

    public void onFragmentResumed(){
        Logger.i("onFragmentResumed of", ""+this.getClass().getSimpleName());
    }
    public BaseActivity getBaseActivity() {
        return this.mActivity;
    }

    protected abstract V getViewModel();

    public interface FragmentCallBack {
        void onAttach();

        void onDetach(String var1);
    }
}
