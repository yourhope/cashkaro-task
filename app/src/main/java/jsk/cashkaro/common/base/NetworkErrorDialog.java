package jsk.cashkaro.common.base;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import jsk.cashkaro.task.R;


/**
 * Created by Sathish on 5/22/2021
 */
public class NetworkErrorDialog extends DialogFragment {

    private FragmentManager fragmentManager;
    private ColorDrawable bgColorDrawable;
    private int animResId = -1;
    private Context context;
    private View contentView;

    public NetworkErrorDialog newInstance() {
        NetworkErrorDialog networkErrorDialog = new NetworkErrorDialog();
        return networkErrorDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.network_error, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return contentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        getDialog().getWindow().setGravity(Gravity.BOTTOM);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(this.bgColorDrawable);
        if (animResId != -1) {
            getDialog().getWindow().getAttributes().windowAnimations = this.animResId;
        }
    }

    public void setWindowStyleAnimation(int animResId) {
        this.animResId = animResId;
    }

    @Nullable
    @Override
    public View getView() {
        return super.getView();
    }

    public void setBackgroundColor(ColorDrawable colorDrawable) {
        this.bgColorDrawable = colorDrawable;
    }

    public void setContentView(FragmentManager fragmentManager, Context context) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void show(FragmentManager fragmentManager, String s) {
        super.show(fragmentManager, s);
    }
}
