package jsk.cashkaro.common.base;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import dagger.android.AndroidInjection;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseIntentService extends JobIntentService {

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

    }
}
