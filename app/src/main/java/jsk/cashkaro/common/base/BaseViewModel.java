package jsk.cashkaro.common.base;

import androidx.databinding.ObservableArrayMap;
import androidx.databinding.ObservableMap;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import io.reactivex.disposables.CompositeDisposable;
import jsk.cashkaro.framework.common.error.BaseException;
import jsk.cashkaro.framework.common.utils.rx.SingleLiveEvent;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseViewModel extends ViewModel {
    private CompositeDisposable mCompositeDisposable;
    private MutableLiveData<Boolean> mIsLoading = new MutableLiveData<>();
    private SingleLiveEvent<Boolean> successState = new SingleLiveEvent<>();
    private SingleLiveEvent<BaseException> errorState = new SingleLiveEvent<>();

    public BaseViewModel() {
        mCompositeDisposable = new CompositeDisposable();
        mIsLoading.setValue(false);
    }

    @Override
    protected void onCleared() {
            mCompositeDisposable.dispose();
        super.onCleared();
    }

    public void clearCompositeDisposable() {
            mCompositeDisposable.clear();
    }

    public LiveData<Boolean> getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.postValue(isLoading);
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public static ObservableMap<String, ObservableMap<String, String>> toObservableMapOfMap(JSONObject jsonObject) throws JSONException {

        ObservableMap<String, ObservableMap<String, String>> rootMap = new ObservableArrayMap<>();
        if (jsonObject != null) {
            Iterator<String> keyIter = jsonObject.keys();
            while (keyIter.hasNext()) {
                String key = keyIter.next();
                Object value = jsonObject.get(key);

                if (value instanceof JSONObject) {
                    value = toObservableMap((JSONObject) value);
                    rootMap.put(key, (ObservableMap<String, String>) value);
                }
            }
        }

        return rootMap;
    }

    public static ObservableMap<String, String> toObservableMap(JSONObject jsonObject) throws JSONException {
        ObservableMap<String, String> innerMap = new ObservableArrayMap<>();
        Iterator<String> keyIter = jsonObject.keys();
        while (keyIter.hasNext()) {
            String key = keyIter.next();
            Object value = jsonObject.get(key);

            if (value instanceof String) {
                innerMap.put(key, (String) value);
            } else if (value instanceof JSONObject || value instanceof JSONArray) {
                innerMap.put(key, value.toString());
            }
        }
        return innerMap;
    }
}
