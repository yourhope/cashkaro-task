package jsk.cashkaro.common.base;

import androidx.databinding.ViewDataBinding;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseBindingFragment<V extends BaseViewModel, T extends ViewDataBinding> extends BaseFragment {
    private T mBinding;

    public T getBinding() {
        return mBinding;
    }

    protected abstract T setUpDataBinding();

}
