package jsk.cashkaro.common.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import jsk.cashkaro.framework.common.utils.logger.Logger;


/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseBindingActivity<V extends BaseViewModel, T extends ViewDataBinding> extends BaseActivity {
    private T mBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mBinding = setupDataBinding();
        } catch (Exception e) {
            Logger.e("exception while setup dataBinding " + e.getMessage());
        }
    }

    protected abstract T setupDataBinding();

    public T getBinding() {
        return mBinding;
    }
}
