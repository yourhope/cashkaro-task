package jsk.cashkaro.common.base;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import dagger.android.support.AndroidSupportInjection;
import jsk.cashkaro.framework.common.utils.logger.Logger;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseDialogFragment<V extends BaseViewModel> extends DialogFragment {
    private BaseActivity mActivity;
    private V mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        mViewModel = getViewModel();
        setHasOptionsMenu(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mActivity = (BaseActivity) context;
            mActivity.onAttach();
        }
    }

    @Override
    public void onDestroy() {
        mActivity = null;
        super.onDestroy();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            Logger.i("exception during show in BaseDialogFragment");
        }
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    protected abstract V getViewModel();

    public interface FragmentCallBack {
        void onAttach();

        void onDetach(String tag);
    }
}
