package jsk.cashkaro.common.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jsk.cashkaro.common.locale.Locale;
import jsk.cashkaro.framework.locale.AbstractLocale;
import jsk.cashkaro.framework.network.controller.ServiceController;
import jsk.cashkaro.task.BuildConfig;
import jsk.cashkaro.task.data.manager.TaskManager;
import jsk.cashkaro.task.data.managerImpl.TaskManagerImpl;
import jsk.cashkaro.task.data.service.AuthService;
import jsk.cashkaro.task.data.service.TaskService;
import retrofit2.Retrofit;

/**
 * Created by Sathish on 5/22/2021
 */
@Module(includes = {UtilsModule.class})
public class AppModule {
    public static final String BASE_URL = "BASE_URL";
    public static final String IS_DEBUG = "isDebug";
    public static final String API_RETROFIT = "API_RETROFIT";
    public static final String INIT_RETROFIT = "INIT_RETROFIT";


    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    GsonBuilder provideGsonBuilder() {
        return new GsonBuilder();
    }

    @Provides
    @Singleton
    @Named(BASE_URL)
    String provideUrl(){
        return BuildConfig.BASE_URL;
    }

    @Provides
    @Singleton
    @Named(IS_DEBUG)
    Boolean provideIsDebug(){
        return BuildConfig.DEBUG;
    }

    @Provides
    @Singleton
    AbstractLocale provideLocale(Context context){
        return Locale.getInstance(context);
    }

    @Singleton
    @Provides
    AuthService provideAuthService(@Named(INIT_RETROFIT) Retrofit retrofit){
        return retrofit.create(AuthService.class);
    }

    @Provides
    @Singleton
    TaskManager provideLoginManagerRepository(ServiceController serviceController, TaskService loginService, Gson gson) {
        return new TaskManagerImpl(serviceController, loginService, gson);
    }

    @Provides
    @Singleton
    TaskService provideLoginService(@Named(API_RETROFIT) Retrofit retrofit){
        return retrofit.create(TaskService.class);
    }
}
