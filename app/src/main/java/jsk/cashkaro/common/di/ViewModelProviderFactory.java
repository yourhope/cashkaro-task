package jsk.cashkaro.common.di;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * Created by Sathish on 5/22/2021
 */
public class ViewModelProviderFactory<V> implements ViewModelProvider.Factory {

    private final V viewModel;

    public ViewModelProviderFactory(V viewModelProviderFactory) {
        this.viewModel = viewModelProviderFactory;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(viewModel.getClass())) {
            return (T) viewModel;
        }
        throw new IllegalArgumentException("unknown class name");
    }
}