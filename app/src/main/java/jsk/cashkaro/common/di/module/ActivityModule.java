package jsk.cashkaro.common.di.module;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import jsk.cashkaro.framework.common.di.annotation.ActivityScope;
import jsk.cashkaro.task.activity.SharedActivity;
import jsk.cashkaro.task.activity.TaskActivity;
import jsk.cashkaro.task.di.SharedActivityModule;
import jsk.cashkaro.task.di.SplashFragmentModule;
import jsk.cashkaro.task.di.TaskActivityModule;
import jsk.cashkaro.task.di.TaskFragmentModule;

/**
 * Created by Sathish on 5/22/2021
 */
@Module
public abstract class ActivityModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = {
            TaskActivityModule.class,
            SplashFragmentModule.SplashFragmentProvider.class,
            TaskFragmentModule.TaskFragmentProvider.class
    })
    abstract TaskActivity bindTaskActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {
            SharedActivityModule.class,
    })
    abstract SharedActivity bindSharedActivity();
}
