package jsk.cashkaro.common.di.navigation;

import androidx.fragment.app.FragmentManager;

import jsk.cashkaro.common.base.BaseActivity;

/**
 * Created by Sathish on 5/22/2021
 */
public abstract class BaseNavigationController implements FragmentManager.OnBackStackChangedListener {

    private static final String TRANSACTION_BACKSTACK_INIT = "init";

    private FragmentManager mFragmentManager;
    private final BaseActivity mContext;

    public BaseNavigationController(BaseActivity activity){
        mContext = activity;
        mFragmentManager = activity.getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);
    }

    public abstract int getContainerId();

    @Override
    public void onBackStackChanged(){

    }

}
