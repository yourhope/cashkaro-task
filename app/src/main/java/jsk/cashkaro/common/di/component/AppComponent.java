package jsk.cashkaro.common.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import jsk.cashkaro.CKApplication;
import jsk.cashkaro.common.di.module.ActivityModule;
import jsk.cashkaro.common.di.module.AppModule;
import jsk.cashkaro.common.di.module.ServiceModule;
import jsk.cashkaro.framework.common.di.CoreFrameworkModule;

/**
 * Created by Sathish on 5/22/2021
 */

@Singleton
@Component(modules = {AppModule.class , AndroidSupportInjectionModule.class , ActivityModule.class , ServiceModule.class, CoreFrameworkModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(CKApplication ckApplication);
}
