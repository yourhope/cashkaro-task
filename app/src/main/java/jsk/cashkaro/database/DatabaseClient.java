package jsk.cashkaro.database;

import android.content.Context;

import androidx.room.Room;

import jsk.cashkaro.common.Constants;

public class DatabaseClient {

    private Context mContext;
    private static DatabaseClient mInstance;

    //our app database object
    private AppDatabase appDatabase;

    private DatabaseClient(Context context) {
        this.mContext = context;

        //creating the app database with Room database builder
        appDatabase = Room.databaseBuilder(mContext, AppDatabase.class, Constants.APP_DATABASE).build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }
}
