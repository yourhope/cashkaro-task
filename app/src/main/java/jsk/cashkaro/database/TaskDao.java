package jsk.cashkaro.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TaskDao {

    @Query("SELECT * FROM taskentity")
    List<TaskEntity> getAll();

    @Insert
    void insert(TaskEntity task);

    @Delete
    void delete(TaskEntity task);

    @Update
    void update(TaskEntity task);

}
